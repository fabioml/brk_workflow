









<META http-equiv="Content-Type" content="text/html; charset=UTF-8">




<style type="text/css">
    table.center {margin-left:auto; margin-right:auto;}
    body {text-align:center;}
</style>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

     
     <html lang="en-US">
     <HEAD>
     <TITLE>Login to Novell Identity Manager</TITLE>
     <META HTTP-EQUIV="pragma" CONTENT="no-cache">
     <META HTTP-EQUIV="cache-control" CONTENT="no-cache">
     <META HTTP-EQUIV="expires" CONTENT="0">


      <link rel='stylesheet' type='text/css' href='/IDMProv/resource/themes/Braskem/print.css' media='print'/>   <link rel='stylesheet' type='text/css' href='/IDMProv/resource/themes/Braskem/theme.css' media='screen'/>   <link rel='stylesheet' type='text/css' href='/IDMProv/themebrand/Braskem/brand.css' media='screen'/>   <link rel='shortcut icon' type='image/x-icon' href='/IDMProv/resource/themes/Braskem/images/favicon.ico' /> 
     </HEAD>

     <BODY
          onload="document.getElementById('LoginForm:userName').focus();document.forms[0].setAttribute('autocomplete', 'off')">
     
<form id="LoginForm" name="LoginForm" method="post" action="/IDMProv/jsps/login/Login.jsf" enctype="application/x-www-form-urlencoded">
<input type="hidden" name="LoginForm" value="LoginForm" />

          <IMG WIDTH="1"
               SRC="/IDMProv/resource/portal-general/images/pixel.gif"
               HEIGHT="50" BORDER="0" ALT="">
          <TABLE ID="loginTable" cols="2" CLASS="nv-borderColor3" CELLSPACING="0"
               CELLPADDING="0" ALIGN="center" width="334px">
               <TR>
     <TD COLSPAN="2" CLASS="nv-loginImage" ALIGN="center"></TD>
</TR>
<TR>
     <TD COLSPAN="2" CLASS="nv-bodyContainer"><IMG
          SRC="/IDMProv/resource/portal-general/images/pixel.gif"
          HEIGHT="8" BORDER="0" ALT=""></TD>
</TR>


               <TR>
                    <TD  COLSPAN="2" CLASS="nv-bodyContainer"
                         ALIGN="center">
                         <TABLE cellspacing="0" cellpadding="0">
						 <tr>
						  <TD colspan="2" align="center">
							<span class="portlet-msg-info"></span>
						  </td>
						 </tr>
                         <TR>
                         <TD>
                         <label class="portlet-form-field-label" for="LoginForm:userName">
                              Username:
                         </label>
                         </TD>
                         <TD>
                         <label class="portlet-form-field-label" for="LoginForm:userPassword">
                              Password:
                         </label>
                         </TD>
                         </TR>
                         <TR>
                         <TD>
                         <input id="LoginForm:userName" type="text" name="LoginForm:userName" class="portlet-form-field-label" />
                         </TD>
                         <TD>
                         <input id="LoginForm:userPassword" type="password" name="LoginForm:userPassword" value="" class="portlet-form-field-label" />
                         </TD>
                         </TR>
                         <TR>
                         <TD colspan="2">
                           
                                  <img id="image" alt=""
                                      src="/IDMProv/resource/portal-general/images/login-arrow.gif">
                                  &nbsp;
                                  <a href="javascript:externalForm.submit();"
                                     class="nv-fontSmall"
                                     >Forgot Password?</a>
                           
                         </TD>
						 
                         </TR>
                         </TABLE>
                    <TR>
     <TD HEIGHT="1" COLSPAN="2" CLASS="nv-backgroundColor4"><IMG
          WIDTH="1"
          SRC="/IDMProv/resource/portal-general/images/pixel.gif"
          HEIGHT="1" BORDER="0" ALT=""></TD>
</TR>
<TR>
     <TD CLASS="nv-backgroundColor10 nv-loginImageSmall" ALIGN="left"></TD>
     <TD CLASS="nv-backgroundColor10" ALIGN="right"><input id="LoginForm:Login" type="submit" name="LoginForm:Login" value="Login..." /></TD>
</TR>

                    </TD>
               </TR>
          </TABLE>
          <table class="center">
<tbody>
<tr>
<td></td>
</tr>
</tbody>
</table>
<input type="hidden" name="javax.faces.ViewState" id="javax.faces.ViewState" value="j_id47:j_id48" />
</form>
     <NOSCRIPT>Your web browser must be JavaScript-enabled to use this application. Update your web browser settings and try again.</NOSCRIPT>

     <form id="externalForm"
          action="/IDMProv/./jsps/pwdmgt/ForgotPassword.jsf"
          method="post"><input type="hidden" id="rtnaddr"
          name="rtnaddr"
          value="" /></form>
</BODY>
</html>





