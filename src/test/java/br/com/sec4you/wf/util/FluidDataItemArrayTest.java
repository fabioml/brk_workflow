package br.com.sec4you.wf.util;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.novell.provisioning.service.DataItem;
import com.novell.provisioning.service.DataItemArray;


public class FluidDataItemArrayTest {

	@Test
	public void test() {
		DataItemArray recod = FluidDataItemArray.init().
			addName("at1").
				addValue("v1").
				addValue("v2").
			addName("at2").
				addValue("v3").
			addName("at3").
				addValue("v5").
				addValue("v4").
				addValue("v6").
			build();
		List<DataItem> itens = recod.getDataitem();
		assertEquals("at1",itens.get(0).getName());
		assertEquals("at2",itens.get(1).getName());
		assertEquals("at3",itens.get(2).getName());
		assertArrayEquals(new String[]{"v1","v2"},itens.get(0).getValue().getString().toArray());
		assertArrayEquals(new String[]{"v3"},itens.get(1).getValue().getString().toArray());
		assertArrayEquals(new String[]{"v5","v4","v6"},itens.get(2).getValue().getString().toArray());
	}
	@Test
	public void test2() {
		DataItemArray recod = FluidDataItemArray.init().
			addName("at1").
				addValue("").
				addValue("").
			addName("at2").
				addValue("v3").			
			build();
		List<DataItem> itens = recod.getDataitem();
		assertEquals("at1",itens.get(0).getName());
		assertEquals("at2",itens.get(1).getName());		
		assertArrayEquals(new String[]{"",""},itens.get(0).getValue().getString().toArray());
		assertArrayEquals(new String[]{"v3"},itens.get(1).getValue().getString().toArray());		
	}
}
