
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getWorkEntriesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getWorkEntriesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.novell.com/provisioning/service}WorkEntryArray"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getWorkEntriesResponse", propOrder = {
    "workEntryArray"
})
public class GetWorkEntriesResponse {

    @XmlElement(name = "WorkEntryArray", required = true)
    protected WorkEntryArray workEntryArray;

    /**
     * Gets the value of the workEntryArray property.
     * 
     * @return
     *     possible object is
     *     {@link WorkEntryArray }
     *     
     */
    public WorkEntryArray getWorkEntryArray() {
        return workEntryArray;
    }

    /**
     * Sets the value of the workEntryArray property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkEntryArray }
     *     
     */
    public void setWorkEntryArray(WorkEntryArray value) {
        this.workEntryArray = value;
    }

}
