
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getQuorumForWorkTaskResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getQuorumForWorkTaskResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.novell.com/provisioning/service}Quorum"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getQuorumForWorkTaskResponse", propOrder = {
    "quorum"
})
public class GetQuorumForWorkTaskResponse {

    @XmlElement(name = "Quorum", required = true)
    protected Quorum quorum;

    /**
     * Gets the value of the quorum property.
     * 
     * @return
     *     possible object is
     *     {@link Quorum }
     *     
     */
    public Quorum getQuorum() {
        return quorum;
    }

    /**
     * Sets the value of the quorum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Quorum }
     *     
     */
    public void setQuorum(Quorum value) {
        this.quorum = value;
    }

}
