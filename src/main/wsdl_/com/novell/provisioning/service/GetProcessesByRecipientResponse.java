
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getProcessesByRecipientResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getProcessesByRecipientResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.novell.com/provisioning/service}ProcessArray"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getProcessesByRecipientResponse", propOrder = {
    "processArray"
})
public class GetProcessesByRecipientResponse {

    @XmlElement(name = "ProcessArray", required = true)
    protected ProcessArray processArray;

    /**
     * Gets the value of the processArray property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessArray }
     *     
     */
    public ProcessArray getProcessArray() {
        return processArray;
    }

    /**
     * Sets the value of the processArray property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessArray }
     *     
     */
    public void setProcessArray(ProcessArray value) {
        this.processArray = value;
    }

}
