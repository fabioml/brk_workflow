
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Configuration complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Configuration">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cleanupInterval" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="completedProcessTimeout" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="emailNotification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="heartbeatFactor" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="heartbeatInterval" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="initialPoolSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="keepAliveTime" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="maxPoolSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="maxShutdownTime" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="minPoolSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="pendingInterval" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="processCacheInitialCapacity" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="processCacheLoadFactor" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="processCacheMaxCapacity" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="retryQueueInterval" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="userActivityTimeout" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="webServiceActivityTimeout" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Configuration", propOrder = {
    "cleanupInterval",
    "completedProcessTimeout",
    "emailNotification",
    "heartbeatFactor",
    "heartbeatInterval",
    "initialPoolSize",
    "keepAliveTime",
    "maxPoolSize",
    "maxShutdownTime",
    "minPoolSize",
    "pendingInterval",
    "processCacheInitialCapacity",
    "processCacheLoadFactor",
    "processCacheMaxCapacity",
    "retryQueueInterval",
    "userActivityTimeout",
    "webServiceActivityTimeout"
})
public class Configuration {

    protected long cleanupInterval;
    protected int completedProcessTimeout;
    protected boolean emailNotification;
    protected int heartbeatFactor;
    protected long heartbeatInterval;
    protected int initialPoolSize;
    protected long keepAliveTime;
    protected int maxPoolSize;
    protected long maxShutdownTime;
    protected int minPoolSize;
    protected long pendingInterval;
    protected int processCacheInitialCapacity;
    protected float processCacheLoadFactor;
    protected int processCacheMaxCapacity;
    protected long retryQueueInterval;
    protected int userActivityTimeout;
    protected int webServiceActivityTimeout;

    /**
     * Gets the value of the cleanupInterval property.
     * 
     */
    public long getCleanupInterval() {
        return cleanupInterval;
    }

    /**
     * Sets the value of the cleanupInterval property.
     * 
     */
    public void setCleanupInterval(long value) {
        this.cleanupInterval = value;
    }

    /**
     * Gets the value of the completedProcessTimeout property.
     * 
     */
    public int getCompletedProcessTimeout() {
        return completedProcessTimeout;
    }

    /**
     * Sets the value of the completedProcessTimeout property.
     * 
     */
    public void setCompletedProcessTimeout(int value) {
        this.completedProcessTimeout = value;
    }

    /**
     * Gets the value of the emailNotification property.
     * 
     */
    public boolean isEmailNotification() {
        return emailNotification;
    }

    /**
     * Sets the value of the emailNotification property.
     * 
     */
    public void setEmailNotification(boolean value) {
        this.emailNotification = value;
    }

    /**
     * Gets the value of the heartbeatFactor property.
     * 
     */
    public int getHeartbeatFactor() {
        return heartbeatFactor;
    }

    /**
     * Sets the value of the heartbeatFactor property.
     * 
     */
    public void setHeartbeatFactor(int value) {
        this.heartbeatFactor = value;
    }

    /**
     * Gets the value of the heartbeatInterval property.
     * 
     */
    public long getHeartbeatInterval() {
        return heartbeatInterval;
    }

    /**
     * Sets the value of the heartbeatInterval property.
     * 
     */
    public void setHeartbeatInterval(long value) {
        this.heartbeatInterval = value;
    }

    /**
     * Gets the value of the initialPoolSize property.
     * 
     */
    public int getInitialPoolSize() {
        return initialPoolSize;
    }

    /**
     * Sets the value of the initialPoolSize property.
     * 
     */
    public void setInitialPoolSize(int value) {
        this.initialPoolSize = value;
    }

    /**
     * Gets the value of the keepAliveTime property.
     * 
     */
    public long getKeepAliveTime() {
        return keepAliveTime;
    }

    /**
     * Sets the value of the keepAliveTime property.
     * 
     */
    public void setKeepAliveTime(long value) {
        this.keepAliveTime = value;
    }

    /**
     * Gets the value of the maxPoolSize property.
     * 
     */
    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    /**
     * Sets the value of the maxPoolSize property.
     * 
     */
    public void setMaxPoolSize(int value) {
        this.maxPoolSize = value;
    }

    /**
     * Gets the value of the maxShutdownTime property.
     * 
     */
    public long getMaxShutdownTime() {
        return maxShutdownTime;
    }

    /**
     * Sets the value of the maxShutdownTime property.
     * 
     */
    public void setMaxShutdownTime(long value) {
        this.maxShutdownTime = value;
    }

    /**
     * Gets the value of the minPoolSize property.
     * 
     */
    public int getMinPoolSize() {
        return minPoolSize;
    }

    /**
     * Sets the value of the minPoolSize property.
     * 
     */
    public void setMinPoolSize(int value) {
        this.minPoolSize = value;
    }

    /**
     * Gets the value of the pendingInterval property.
     * 
     */
    public long getPendingInterval() {
        return pendingInterval;
    }

    /**
     * Sets the value of the pendingInterval property.
     * 
     */
    public void setPendingInterval(long value) {
        this.pendingInterval = value;
    }

    /**
     * Gets the value of the processCacheInitialCapacity property.
     * 
     */
    public int getProcessCacheInitialCapacity() {
        return processCacheInitialCapacity;
    }

    /**
     * Sets the value of the processCacheInitialCapacity property.
     * 
     */
    public void setProcessCacheInitialCapacity(int value) {
        this.processCacheInitialCapacity = value;
    }

    /**
     * Gets the value of the processCacheLoadFactor property.
     * 
     */
    public float getProcessCacheLoadFactor() {
        return processCacheLoadFactor;
    }

    /**
     * Sets the value of the processCacheLoadFactor property.
     * 
     */
    public void setProcessCacheLoadFactor(float value) {
        this.processCacheLoadFactor = value;
    }

    /**
     * Gets the value of the processCacheMaxCapacity property.
     * 
     */
    public int getProcessCacheMaxCapacity() {
        return processCacheMaxCapacity;
    }

    /**
     * Sets the value of the processCacheMaxCapacity property.
     * 
     */
    public void setProcessCacheMaxCapacity(int value) {
        this.processCacheMaxCapacity = value;
    }

    /**
     * Gets the value of the retryQueueInterval property.
     * 
     */
    public long getRetryQueueInterval() {
        return retryQueueInterval;
    }

    /**
     * Sets the value of the retryQueueInterval property.
     * 
     */
    public void setRetryQueueInterval(long value) {
        this.retryQueueInterval = value;
    }

    /**
     * Gets the value of the userActivityTimeout property.
     * 
     */
    public int getUserActivityTimeout() {
        return userActivityTimeout;
    }

    /**
     * Sets the value of the userActivityTimeout property.
     * 
     */
    public void setUserActivityTimeout(int value) {
        this.userActivityTimeout = value;
    }

    /**
     * Gets the value of the webServiceActivityTimeout property.
     * 
     */
    public int getWebServiceActivityTimeout() {
        return webServiceActivityTimeout;
    }

    /**
     * Sets the value of the webServiceActivityTimeout property.
     * 
     */
    public void setWebServiceActivityTimeout(int value) {
        this.webServiceActivityTimeout = value;
    }

}
