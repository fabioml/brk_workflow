
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_processInfoOrder.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="t_processInfoOrder">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PROCESS_ID"/>
 *     &lt;enumeration value="REQUEST_ID"/>
 *     &lt;enumeration value="APPROVAL_STATUS"/>
 *     &lt;enumeration value="PROCESS_STATUS"/>
 *     &lt;enumeration value="CREATION_TIME"/>
 *     &lt;enumeration value="COMPLETION_TIME"/>
 *     &lt;enumeration value="INITIATOR"/>
 *     &lt;enumeration value="RECIPIENT"/>
 *     &lt;enumeration value="RESOURCE_TYPE"/>
 *     &lt;enumeration value="ENGINE_ID"/>
 *     &lt;enumeration value="CORRELATION_ID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "t_processInfoOrder")
@XmlEnum
public enum TProcessInfoOrder {

    PROCESS_ID,
    REQUEST_ID,
    APPROVAL_STATUS,
    PROCESS_STATUS,
    CREATION_TIME,
    COMPLETION_TIME,
    INITIATOR,
    RECIPIENT,
    RESOURCE_TYPE,
    ENGINE_ID,
    CORRELATION_ID;

    public String value() {
        return name();
    }

    public static TProcessInfoOrder fromValue(String v) {
        return valueOf(v);
    }

}
