
package com.novell.provisioning.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_processInfoQuery complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_processInfoQuery">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="processId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="requestId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="engineId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="recipient" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="initiator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="approvalStatus" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="processStatus" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="resourceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="creationTime" type="{http://www.novell.com/provisioning/service}t_time"/>
 *         &lt;element name="completionTime" type="{http://www.novell.com/provisioning/service}t_time"/>
 *         &lt;element name="correlationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/choice>
 *       &lt;attribute name="logic" type="{http://www.novell.com/provisioning/service}t_logic" />
 *       &lt;attribute name="order" type="{http://www.novell.com/provisioning/service}t_processInfoOrder" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_processInfoQuery", propOrder = {
    "processIdOrRequestIdOrEngineId"
})
public class TProcessInfoQuery {

    @XmlElementRefs({
        @XmlElementRef(name = "creationTime", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "completionTime", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "requestId", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "engineId", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "processId", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "recipient", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "processStatus", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "approvalStatus", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "resourceType", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "initiator", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "correlationId", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class)
    })
    protected List<JAXBElement<?>> processIdOrRequestIdOrEngineId;
    @XmlAttribute
    protected TLogic logic;
    @XmlAttribute
    protected TProcessInfoOrder order;

    /**
     * Gets the value of the processIdOrRequestIdOrEngineId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the processIdOrRequestIdOrEngineId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProcessIdOrRequestIdOrEngineId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link TTime }{@code >}
     * {@link JAXBElement }{@code <}{@link TTime }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getProcessIdOrRequestIdOrEngineId() {
        if (processIdOrRequestIdOrEngineId == null) {
            processIdOrRequestIdOrEngineId = new ArrayList<JAXBElement<?>>();
        }
        return this.processIdOrRequestIdOrEngineId;
    }

    /**
     * Gets the value of the logic property.
     * 
     * @return
     *     possible object is
     *     {@link TLogic }
     *     
     */
    public TLogic getLogic() {
        return logic;
    }

    /**
     * Sets the value of the logic property.
     * 
     * @param value
     *     allowed object is
     *     {@link TLogic }
     *     
     */
    public void setLogic(TLogic value) {
        this.logic = value;
    }

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link TProcessInfoOrder }
     *     
     */
    public TProcessInfoOrder getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link TProcessInfoOrder }
     *     
     */
    public void setOrder(TProcessInfoOrder value) {
        this.order = value;
    }

}
