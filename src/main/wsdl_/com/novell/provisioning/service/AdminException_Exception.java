
package com.novell.provisioning.service;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.2.3
 * Wed Apr 02 00:09:16 BRT 2014
 * Generated source version: 2.2.3
 * 
 */

@WebFault(name = "AdminException", targetNamespace = "http://www.novell.com/provisioning/service")
public class AdminException_Exception extends Exception {
    public static final long serialVersionUID = 20140402000916L;
    
    private com.novell.provisioning.service.AdminException adminException;

    public AdminException_Exception() {
        super();
    }
    
    public AdminException_Exception(String message) {
        super(message);
    }
    
    public AdminException_Exception(String message, Throwable cause) {
        super(message, cause);
    }

    public AdminException_Exception(String message, com.novell.provisioning.service.AdminException adminException) {
        super(message);
        this.adminException = adminException;
    }

    public AdminException_Exception(String message, com.novell.provisioning.service.AdminException adminException, Throwable cause) {
        super(message, cause);
        this.adminException = adminException;
    }

    public com.novell.provisioning.service.AdminException getFaultInfo() {
        return this.adminException;
    }
}
