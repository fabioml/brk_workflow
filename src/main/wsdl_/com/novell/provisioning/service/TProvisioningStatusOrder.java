
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_provisioningStatusOrder.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="t_provisioningStatusOrder">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ACTIVITY_ID"/>
 *     &lt;enumeration value="RECIPIENT"/>
 *     &lt;enumeration value="PROVISIONING_TIME"/>
 *     &lt;enumeration value="RESULT_TIME"/>
 *     &lt;enumeration value="STATE"/>
 *     &lt;enumeration value="STATUS"/>
 *     &lt;enumeration value="REQUEST_ID"/>
 *     &lt;enumeration value="MESSAGE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "t_provisioningStatusOrder")
@XmlEnum
public enum TProvisioningStatusOrder {

    ACTIVITY_ID,
    RECIPIENT,
    PROVISIONING_TIME,
    RESULT_TIME,
    STATE,
    STATUS,
    REQUEST_ID,
    MESSAGE;

    public String value() {
        return name();
    }

    public static TProvisioningStatusOrder fromValue(String v) {
        return valueOf(v);
    }

}
