
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_action.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="t_action">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="APPROVE"/>
 *     &lt;enumeration value="DENY"/>
 *     &lt;enumeration value="REFUSE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "t_action")
@XmlEnum
public enum TAction {

    APPROVE,
    DENY,
    REFUSE;

    public String value() {
        return name();
    }

    public static TAction fromValue(String v) {
        return valueOf(v);
    }

}
