
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for EngineState complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EngineState">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="engineId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="heartbeat" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="startTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="shutdownTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="engineStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="valueOfEngineStatus" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EngineState", propOrder = {
    "engineId",
    "heartbeat",
    "startTime",
    "shutdownTime",
    "engineStatus",
    "valueOfEngineStatus"
})
public class EngineState {

    @XmlElement(required = true)
    protected String engineId;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar heartbeat;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startTime;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar shutdownTime;
    @XmlElement(required = true)
    protected String engineStatus;
    protected int valueOfEngineStatus;

    /**
     * Gets the value of the engineId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEngineId() {
        return engineId;
    }

    /**
     * Sets the value of the engineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEngineId(String value) {
        this.engineId = value;
    }

    /**
     * Gets the value of the heartbeat property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHeartbeat() {
        return heartbeat;
    }

    /**
     * Sets the value of the heartbeat property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHeartbeat(XMLGregorianCalendar value) {
        this.heartbeat = value;
    }

    /**
     * Gets the value of the startTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartTime() {
        return startTime;
    }

    /**
     * Sets the value of the startTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartTime(XMLGregorianCalendar value) {
        this.startTime = value;
    }

    /**
     * Gets the value of the shutdownTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getShutdownTime() {
        return shutdownTime;
    }

    /**
     * Sets the value of the shutdownTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setShutdownTime(XMLGregorianCalendar value) {
        this.shutdownTime = value;
    }

    /**
     * Gets the value of the engineStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEngineStatus() {
        return engineStatus;
    }

    /**
     * Sets the value of the engineStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEngineStatus(String value) {
        this.engineStatus = value;
    }

    /**
     * Gets the value of the valueOfEngineStatus property.
     * 
     */
    public int getValueOfEngineStatus() {
        return valueOfEngineStatus;
    }

    /**
     * Sets the value of the valueOfEngineStatus property.
     * 
     */
    public void setValueOfEngineStatus(int value) {
        this.valueOfEngineStatus = value;
    }

}
