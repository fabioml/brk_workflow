
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for setResultRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="setResultRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{http://www.novell.com/provisioning/service}EntitlementState"/>
 *         &lt;element ref="{http://www.novell.com/provisioning/service}EntitlementStatus"/>
 *         &lt;element name="arg3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "setResultRequest", propOrder = {
    "arg0",
    "entitlementState",
    "entitlementStatus",
    "arg3"
})
public class SetResultRequest {

    @XmlElement(nillable = true)
    protected String arg0;
    @XmlElement(name = "EntitlementState", required = true)
    protected TEntitlementState entitlementState;
    @XmlElement(name = "EntitlementStatus", required = true)
    protected TEntitlementStatus entitlementStatus;
    @XmlElement(nillable = true)
    protected String arg3;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArg0(String value) {
        this.arg0 = value;
    }

    /**
     * Gets the value of the entitlementState property.
     * 
     * @return
     *     possible object is
     *     {@link TEntitlementState }
     *     
     */
    public TEntitlementState getEntitlementState() {
        return entitlementState;
    }

    /**
     * Sets the value of the entitlementState property.
     * 
     * @param value
     *     allowed object is
     *     {@link TEntitlementState }
     *     
     */
    public void setEntitlementState(TEntitlementState value) {
        this.entitlementState = value;
    }

    /**
     * Gets the value of the entitlementStatus property.
     * 
     * @return
     *     possible object is
     *     {@link TEntitlementStatus }
     *     
     */
    public TEntitlementStatus getEntitlementStatus() {
        return entitlementStatus;
    }

    /**
     * Sets the value of the entitlementStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link TEntitlementStatus }
     *     
     */
    public void setEntitlementStatus(TEntitlementStatus value) {
        this.entitlementStatus = value;
    }

    /**
     * Gets the value of the arg3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArg3() {
        return arg3;
    }

    /**
     * Sets the value of the arg3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArg3(String value) {
        this.arg3 = value;
    }

}
