
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_commentType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="t_commentType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="User"/>
 *     &lt;enumeration value="System"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "t_commentType")
@XmlEnum
public enum TCommentType {

    @XmlEnumValue("User")
    USER("User"),
    @XmlEnumValue("System")
    SYSTEM("System");
    private final String value;

    TCommentType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TCommentType fromValue(String v) {
        for (TCommentType c: TCommentType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
