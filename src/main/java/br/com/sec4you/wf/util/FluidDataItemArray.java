package br.com.sec4you.wf.util;

import java.util.Arrays;

import com.google.common.base.Preconditions;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import com.novell.provisioning.service.DataItem;
import com.novell.provisioning.service.DataItemArray;
import com.novell.provisioning.service.StringArray;


public class FluidDataItemArray {
	Multimap<String, String> mult;
	String lastName;
	private FluidDataItemArray(){
		lastName = "";
		mult = LinkedListMultimap.create();
	}
	
	public static FluidDataItemArray init(){
		return new FluidDataItemArray();
	}
	public FluidDataItemArray addName(String value){
		Preconditions.checkNotNull(value);
		Preconditions.checkArgument(value.length()!=0);
		
		lastName = value;
		return this;
	}
	public FluidDataItemArray addValue(String value){
		Preconditions.checkNotNull(value);		
		Preconditions.checkState(lastName.length()!=0);
		
		mult.put(lastName, value);
		return this;
	}
	public DataItemArray build(){
		DataItemArray array = new DataItemArray();
		DataItem[] arrayItems = new DataItem[mult.keys().size()];
		int i=0;
		for (String name : mult.keySet()){			
			StringArray values = new StringArray();
			values.getString().addAll(mult.get(name));
			
			DataItem item = new DataItem();
			item.setName(name);			
			item.setValue(values);
			arrayItems[i++] = item;
		}
		array.getDataitem().addAll(Arrays.asList(arrayItems));
		return array;
	}
}
