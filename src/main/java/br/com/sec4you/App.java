package br.com.sec4you;

import java.net.MalformedURLException;
import java.util.Collections;
import java.util.List;

import com.google.common.base.Optional;
import com.novell.provisioning.service.AdminException;
import com.novell.provisioning.service.AdminException_Exception;
import com.novell.provisioning.service.DataItem;
import com.novell.provisioning.service.DataItemArray;
import com.novell.provisioning.service.Process;
import com.novell.provisioning.service.ProvisioningRequest;
import com.novell.provisioning.service.TApprovalStatus;
import com.novell.provisioning.service.TProcessStatus;
import com.novell.provisioning.service.WorkEntry;

import br.com.sec4you.fw.Provision;
import br.com.sec4you.wf.util.FluidDataItemArray;

/**
 * Hello world!
 *
 */
public class App 
{	
    public static void main( String[] args )
    {        
    	try {
        	//final String url = "http://10.250.252.126:8081/IDMProv/provisioning/service";
        	final String url = "http://10.250.252.124:8081/IDMProv/provisioning/service";
        	final String userDN ="CN=provadmin,OU=ACCOUNTS,OU=SERVICES,O=MID";
        	final String pwd ="N0v3ll" ;
        	Provision prov = new Provision(url, userDN, pwd); 	        
        	
        	//Optional<String[]> s = prov.getProvisioningCategories();
        	//brask@@1
        	/*FluidDataItemArray f = FluidDataItemArray.init()
        			.addName("approver").addValue("cn=luizju09,ou=ID,o=MID")
        			.addName("toDate").addValue("2014-05-28")
        			.addName("reason").addValue("quero fazer a expansao")
        			.addName("workflowName").addValue("BMeu WF");
        			/*.addName("DataAtivacao").addValue("")
        			.addName("DataExpiração").addValue("")
        			.addName("Justificativa").addValue("sei nao")
        			.addName("SoD").addValue("")
        			.addName("SoD2").addValue("")
        			.addName("areaDeNegocio").addValue("")
        			.addName("descricao").addValue("descricao vai la")
        			.addName("filtro").addValue("")
        			.addName("logindest").addValue("")
        			.addName("loginlider").addValue("")
        			.addName("nomelider").addValue("")
        			.addName("recipient").addValue("CN=joaoju01,OU=ID,O=MID")
        			.addName("roles").addValue("")
        			.addName("rolesUser").addValue("")
        			.addName("sistema").addValue("")
        			.addName("tipcol").addValue("")
        			.addName("treinamentosNecessarios").addValue("")
        			.addName("treinamentosRealizados").addValue("")
        			.addName("typeSystem").addValue("");*/        
	        //  prov.startWorkflow("cn=Workflow de Prorrogacao,cn=RequestDefs,cn=AppConfig,cn=UserApplication,cn=DriverSet,ou=IDM,ou=SERVICES,o=MID", "CN=joaoju01,OU=ID,O=MID", f.build());
          
        	
        	
        	
	        /*Optional<Process[]> b = prov.getAllProcesses();
	        if (!b.isPresent()){
	        	System.out.println("nenhum processo encontrado");
	        	System.exit(-1);
	        }
	        int i = 0;
	        String firstRecipient ="";
	        for (Process proc : b.get()){
	        	System.out.println("ProcessName: "+proc.getProcessName());
	        	System.out.println("ProcessId: "+proc.getProcessId());
	        	if (i == 0)
	        		firstRecipient = proc.getRecipient();
	        	System.out.println(proc.getRecipient());
	        	if ( i++ > 20 )
	        		break;
	        }	  */      
	        
	        
	        
	        
	        
	        
	        
	        /*Optional<List<ProvisioningRequest>> recipientRequests = prov.getAllProvisioningRequests("cn=luizju09,ou=ID,o=MID");
	        if (!recipientRequests.isPresent()){
	        	System.out.println("nenhum processo encontrado para o recipient");
	        	System.exit(-1);
	        }
	        for (ProvisioningRequest recipientReq : recipientRequests.get()){
	        	System.out.println("Name: "+recipientReq.getName());
	        	System.out.println("ID: "+recipientReq.getId());
	        	DataItemArray items = recipientReq.getItems();
	        	List<DataItem> item = items.getDataitem();
	        	for (DataItem it : item){	        	
	        		System.out.print(it.getName() + " -> ");
	        		for (String s : it.getValue().getString())
	        			System.out.println(s);
	        	}
	        	
	        }*/
        	
        	List<WorkEntry> wks = prov.getWork("cn=luizju09,ou=ID,o=MID");
        	for (WorkEntry wk : wks){
	        	System.out.println(wk.getActivityName());	        	
	        	System.out.println(wk.getAddressee());
	        	System.out.println(wk.getRecipient());
	        	System.out.println(wk.getActivityId());
	        	System.out.println(wk.getId());
	        	System.out.println(wk.getProcessId());
	        	System.out.println(wk.getRequestId());
	        }
        	
        	/*Optional<List<Process>> process = prov.getProcessesByStatus(TProcessStatus.RUNNING);
        	Optional<List<Process>> process = prov.getProcessesByRecipient("cn=luizju09,ou=ID,o=MID");
        	if (!process.isPresent()){
	        	System.out.println("nenhum processo encontrado para o recipient");
	        	System.exit(-1);
	        }
	        for (Process p : process.get()){
	        	System.out.println(prov.toStringProcess(p));	        	
	        }*/
	       
	        
	        System.out.println("fim");
      	} catch (AdminException_Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
          AdminException info = e.getFaultInfo();
          System.out.println(info.getReason());
        } catch (MalformedURLException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
    }
}
