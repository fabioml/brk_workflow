package br.com.sec4you.fw;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/** Interface inutil por enquanto
 * Sera utilizada para fazer os testes de webservice dpeois
 * @author fabio
 *
 */
@WebService
public interface TestService {
	@WebMethod(operationName = "gerarLogin")
  public String loginGenerate(@WebParam(name = "nome") String name, @WebParam(name = "sobrenome") String surname);
}
