package br.com.sec4you.fw;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.Conduit;
import org.apache.cxf.transport.http.HTTPConduit;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.novell.provisioning.service.AdminException_Exception;
import com.novell.provisioning.service.DataItemArray;
import com.novell.provisioning.service.GetAllProcessesRequest;
import com.novell.provisioning.service.GetAllProcessesResponse;
import com.novell.provisioning.service.GetAllProvisioningRequestsRequest;
import com.novell.provisioning.service.GetAllProvisioningRequestsResponse;
import com.novell.provisioning.service.GetProcessesByApprovalStatusRequest;
import com.novell.provisioning.service.GetProcessesByApprovalStatusResponse;
import com.novell.provisioning.service.GetProcessesByQueryRequest;
import com.novell.provisioning.service.GetProcessesByQueryResponse;
import com.novell.provisioning.service.GetProcessesByRecipientRequest;
import com.novell.provisioning.service.GetProcessesByRecipientResponse;
import com.novell.provisioning.service.GetProcessesByStatusRequest;
import com.novell.provisioning.service.GetProcessesByStatusResponse;
import com.novell.provisioning.service.GetProvisioningCategoriesRequest;
import com.novell.provisioning.service.GetProvisioningCategoriesResponse;
import com.novell.provisioning.service.GetWorkEntriesRequest;
import com.novell.provisioning.service.GetWorkEntriesResponse;
import com.novell.provisioning.service.Process;
import com.novell.provisioning.service.ProcessArray;
import com.novell.provisioning.service.Provisioning;
import com.novell.provisioning.service.ProvisioningRequest;
import com.novell.provisioning.service.ProvisioningRequestArray;
import com.novell.provisioning.service.ProvisioningService;
import com.novell.provisioning.service.StartRequest;
import com.novell.provisioning.service.StartResponse;
import com.novell.provisioning.service.StringArray;
import com.novell.provisioning.service.TApprovalStatus;
import com.novell.provisioning.service.TLogic;
import com.novell.provisioning.service.TProcessInfoOrder;
import com.novell.provisioning.service.TProcessInfoQuery;
import com.novell.provisioning.service.TProcessStatus;
import com.novell.provisioning.service.TWorkEntryQuery;
import com.novell.provisioning.service.WorkEntry;
import com.novell.provisioning.service.WorkEntryArray;


public class Provision {

  final Provisioning stub;
	
	public Provision(String url, String dn, String pwd) throws MalformedURLException{
		URL wsdlURL = new URL(url + "?wsdl");
		ProvisioningService service = new ProvisioningService(wsdlURL);
		stub = service.getProvisioningPort();
		Client client = ClientProxy.getClient(stub);
		HTTPConduit http = (HTTPConduit) client.getConduit();
		http.getAuthorization().setUserName(dn);
		http.getAuthorization().setPassword(pwd);
	}
	
	public Optional<List<com.novell.provisioning.service.Process>> getAllProcesses() throws AdminException_Exception 
	{
		GetAllProcessesRequest bodyIn = new GetAllProcessesRequest();
		GetAllProcessesResponse response = stub.getAllProcesses(bodyIn );
		if (response == null) return Optional.absent();
		ProcessArray p = response.getProcessArray();
		if (p == null) return Optional.absent();
		return Optional.fromNullable(p.getProcess());
	}
	/**
	 * Cria um wf utilizando os parametros abaixo
	 * 
	 * @param wfName
	 * @param recipient
	 * @param params utilize a classe FluidDataItemArray para facilitar a criacao desse atributo
	 * @throws RemoteException
	 * @throws AdminException_Exception 
	 */
	public String startWorkflow(String wfName, String recipient, DataItemArray params) throws  AdminException_Exception{
		/*Optional<ProvisioningRequest[]> wfRequests = getAllProvisioningRequests(recipient);
		if (!wfRequests.isPresent())
			throw new MissingResourceException("Não foi encontrado nenhum wf para o usuario", "WorkflowName", recipient);
		Optional<ProvisioningRequest> wfRequest = findRequest(wfName, wfRequests.get());
		if (!wfRequest.isPresent())
			throw new MissingResourceException("Não foi encontrado o WF pelo nome", "WorkflowName", wfName);
		*/
		
		StartRequest bodyIn = new StartRequest();
		bodyIn.setArg0(wfName);
		bodyIn.setArg1(recipient);
		bodyIn.setArg2(params);
		
		StartResponse resp = stub.start(bodyIn);
		return resp.getResult();		
	}
	
	public Optional<List<ProvisioningRequest>> getAllProvisioningRequests(String recipient) throws  AdminException_Exception{
		GetAllProvisioningRequestsRequest bodyIn = new GetAllProvisioningRequestsRequest();
		bodyIn.setArg0(recipient);
		GetAllProvisioningRequestsResponse resp = stub.getAllProvisioningRequests(bodyIn );
		if (resp == null) return Optional.absent();
		ProvisioningRequestArray array = resp.getResult();
		if (array == null) return Optional.absent();
		return Optional.fromNullable(array.getProvisioningrequest());		
	}
	
	public Optional<List<String>> getProvisioningCategories() throws  AdminException_Exception{
		
		GetProvisioningCategoriesRequest bodyIn = new GetProvisioningCategoriesRequest();		
		GetProvisioningCategoriesResponse resp = stub.getProvisioningCategories(bodyIn );
		StringArray a = resp.getResult();
		return Optional.fromNullable(a.getString());	

	}
	Optional<ProvisioningRequest> findRequest(final String fwName, ProvisioningRequest[] array){
		
		return Iterables.tryFind(Arrays.asList(array), new Predicate<ProvisioningRequest>(){

			public boolean apply(ProvisioningRequest prov) {
	      return prov.getName().equalsIgnoreCase(fwName);	      
      }
			
		});		
	}
	
	public Optional<List<Process>> getProcessesByApprovalStatus(TApprovalStatus st) throws AdminException_Exception{
		GetProcessesByApprovalStatusRequest status = new GetProcessesByApprovalStatusRequest();
		status.setApprovalStatus(st);
		GetProcessesByApprovalStatusResponse resp =  stub.getProcessesByApprovalStatus(status);
		ProcessArray process = resp.getProcessArray();
		return Optional.fromNullable(process.getProcess());
	}
	
	public Optional<List<Process>> getProcessesByStatus(TProcessStatus st) throws AdminException_Exception{
		GetProcessesByStatusRequest status = new GetProcessesByStatusRequest ();
		status.setProcessStatus(st);
		GetProcessesByStatusResponse resp = stub.getProcessesByStatus(status );
		ProcessArray array = resp.getProcessArray();
		return Optional.fromNullable(array.getProcess());				
	}
	
	public Optional<List<Process>> getProcessesByRecipient(String recipientStr) throws AdminException_Exception {		
		GetProcessesByRecipientRequest recipent = new GetProcessesByRecipientRequest ();
		recipent.setArg0(recipientStr);
		GetProcessesByRecipientResponse resp = stub.getProcessesByRecipient(recipent);
		ProcessArray array = resp.getProcessArray();
		
		return Optional.fromNullable(array.getProcess());
	}
	
	public Optional<List<Process>> getProcessesByQuery(String recipientStr) throws AdminException_Exception {		
		GetProcessesByQueryRequest query = new GetProcessesByQueryRequest();
		TProcessInfoQuery info = new TProcessInfoQuery();		
		info.setLogic(TLogic.AND);			
		info.setOrder(TProcessInfoOrder.REQUEST_ID);
		//http://blog.bdoughan.com/2012/08/removing-jaxbelement-from-your-domain.html
		//info.getProcessIdOrRequestIdOrEngineId().add(new JAXBElement<String>("aaa"));
		query.setProcessInfoQuery(info);
		query.setArg1(10);//maxRecords
		GetProcessesByQueryResponse processArray = stub.getProcessesByQuery(query );
		if (processArray == null) return Optional.absent();
		ProcessArray array = processArray.getProcessArray();
		if (array == null) return Optional.absent();
		return Optional.fromNullable(array.getProcess());
	}
	
	public List<WorkEntry> getWork (String workId) throws AdminException_Exception {
		GetWorkEntriesRequest request = new GetWorkEntriesRequest();
		TWorkEntryQuery info = new TWorkEntryQuery();
		List<JAXBElement<?>> list = info.getAddresseeOrProcessIdOrRequestId();
		JAXBElement<String> elem1 = new JAXBElement<String>(new QName("http://www.novell.com/provisioning/service","recipient"),String.class,null,workId);
		JAXBElement<String> elem2 = new JAXBElement<String>(new QName("http://www.novell.com/provisioning/service","initiator"),String.class,null,workId);
		JAXBElement<Integer> elem3 = new JAXBElement<Integer>(new QName("http://www.novell.com/provisioning/service","status"),Integer.class,null,0);
		list.add(elem1 );
		list.add(elem2 );
		list.add(elem3 );
		info.setLogic(TLogic.AND);
		request.setWorkEntryQuery(info);
		request.setArg1(-1);
		GetWorkEntriesResponse wks = stub.getWorkEntries(request );
		WorkEntryArray array = wks.getWorkEntryArray();
		return array.getWorkentry();
	}

	public String toStringProcess(Process p){
		StringBuffer buff = new StringBuffer();
		buff.append("getProcessName: " + p.getProcessName()).append("\n");
  	buff.append("getApprovalStatus: " + p.getApprovalStatus()).append("\n");		
		buff.append("getProcessStatus: " + p.getProcessStatus()).append("\n");
  	buff.append("getCorrelationId: " + p.getCorrelationId()).append("\n");
  	buff.append("getEngineId: " + p.getEngineId()).append("\n");
  	buff.append("getProcessId: " + p.getProcessId()).append("\n");
  	buff.append("getRequestId: " + p.getRequestId()).append("\n");
  	buff.append("getInitiator: " + p.getInitiator()).append("\n");
  	buff.append("getRecipient: " + p.getRecipient()).append("\n");
  	buff.append("getValueOfApprovalStatus: " + p.getValueOfApprovalStatus()).append("\n");
  	buff.append("getValueOfProcessStatus: " + p.getValueOfProcessStatus()).append("\n");
  	buff.append("getVersion: " + p.getVersion()).append("\n");
  	buff.append("getCreationTime: " + p.getCreationTime()).append("\n");
  	buff.append("getCompletionTime: " + p.getCompletionTime()).append("\n");
  	buff.append("getProxy: " + p.getProxy()).append("\n");
  	return buff.toString();
	}
}		

