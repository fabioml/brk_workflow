
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AvailableAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AvailableAction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="actionCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="actionValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="digitalSignatureRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="digitalSignatureType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AvailableAction", propOrder = {
    "actionCode",
    "actionValue",
    "digitalSignatureRequired",
    "digitalSignatureType"
})
public class AvailableAction {

    protected int actionCode;
    @XmlElement(required = true)
    protected String actionValue;
    protected boolean digitalSignatureRequired;
    @XmlElement(required = true)
    protected String digitalSignatureType;

    /**
     * Gets the value of the actionCode property.
     * 
     */
    public int getActionCode() {
        return actionCode;
    }

    /**
     * Sets the value of the actionCode property.
     * 
     */
    public void setActionCode(int value) {
        this.actionCode = value;
    }

    /**
     * Gets the value of the actionValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionValue() {
        return actionValue;
    }

    /**
     * Sets the value of the actionValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionValue(String value) {
        this.actionValue = value;
    }

    /**
     * Gets the value of the digitalSignatureRequired property.
     * 
     */
    public boolean isDigitalSignatureRequired() {
        return digitalSignatureRequired;
    }

    /**
     * Sets the value of the digitalSignatureRequired property.
     * 
     */
    public void setDigitalSignatureRequired(boolean value) {
        this.digitalSignatureRequired = value;
    }

    /**
     * Gets the value of the digitalSignatureType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDigitalSignatureType() {
        return digitalSignatureType;
    }

    /**
     * Sets the value of the digitalSignatureType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDigitalSignatureType(String value) {
        this.digitalSignatureType = value;
    }

}
