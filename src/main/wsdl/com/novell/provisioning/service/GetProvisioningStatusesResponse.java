
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getProvisioningStatusesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getProvisioningStatusesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.novell.com/provisioning/service}ProvisioningStatusArray"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getProvisioningStatusesResponse", propOrder = {
    "provisioningStatusArray"
})
public class GetProvisioningStatusesResponse {

    @XmlElement(name = "ProvisioningStatusArray", required = true)
    protected ProvisioningStatusArray provisioningStatusArray;

    /**
     * Gets the value of the provisioningStatusArray property.
     * 
     * @return
     *     possible object is
     *     {@link ProvisioningStatusArray }
     *     
     */
    public ProvisioningStatusArray getProvisioningStatusArray() {
        return provisioningStatusArray;
    }

    /**
     * Sets the value of the provisioningStatusArray property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProvisioningStatusArray }
     *     
     */
    public void setProvisioningStatusArray(ProvisioningStatusArray value) {
        this.provisioningStatusArray = value;
    }

}
