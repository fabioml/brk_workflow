
package com.novell.provisioning.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_provisioningStatusQuery complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_provisioningStatusQuery">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="recipient" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="requestId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="activityId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="provisioningTime" type="{http://www.novell.com/provisioning/service}t_time"/>
 *         &lt;element name="resultTime" type="{http://www.novell.com/provisioning/service}t_time"/>
 *       &lt;/choice>
 *       &lt;attribute name="logic" type="{http://www.novell.com/provisioning/service}t_logic" />
 *       &lt;attribute name="order" type="{http://www.novell.com/provisioning/service}t_provisioningStatusOrder" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_provisioningStatusQuery", propOrder = {
    "recipientOrRequestIdOrActivityId"
})
public class TProvisioningStatusQuery {

    @XmlElementRefs({
        @XmlElementRef(name = "state", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "activityId", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "status", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "provisioningTime", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "resultTime", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "requestId", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class),
        @XmlElementRef(name = "recipient", namespace = "http://www.novell.com/provisioning/service", type = JAXBElement.class)
    })
    protected List<JAXBElement<?>> recipientOrRequestIdOrActivityId;
    @XmlAttribute
    protected TLogic logic;
    @XmlAttribute
    protected TProvisioningStatusOrder order;

    /**
     * Gets the value of the recipientOrRequestIdOrActivityId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the recipientOrRequestIdOrActivityId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRecipientOrRequestIdOrActivityId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * {@link JAXBElement }{@code <}{@link TTime }{@code >}
     * {@link JAXBElement }{@code <}{@link TTime }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getRecipientOrRequestIdOrActivityId() {
        if (recipientOrRequestIdOrActivityId == null) {
            recipientOrRequestIdOrActivityId = new ArrayList<JAXBElement<?>>();
        }
        return this.recipientOrRequestIdOrActivityId;
    }

    /**
     * Gets the value of the logic property.
     * 
     * @return
     *     possible object is
     *     {@link TLogic }
     *     
     */
    public TLogic getLogic() {
        return logic;
    }

    /**
     * Sets the value of the logic property.
     * 
     * @param value
     *     allowed object is
     *     {@link TLogic }
     *     
     */
    public void setLogic(TLogic value) {
        this.logic = value;
    }

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link TProvisioningStatusOrder }
     *     
     */
    public TProvisioningStatusOrder getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link TProvisioningStatusOrder }
     *     
     */
    public void setOrder(TProvisioningStatusOrder value) {
        this.order = value;
    }

}
