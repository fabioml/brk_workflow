
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getProcessesByQueryRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getProcessesByQueryRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.novell.com/provisioning/service}ProcessInfoQuery"/>
 *         &lt;element name="arg1" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getProcessesByQueryRequest", propOrder = {
    "processInfoQuery",
    "arg1"
})
public class GetProcessesByQueryRequest {

    @XmlElement(name = "ProcessInfoQuery", required = true)
    protected TProcessInfoQuery processInfoQuery;
    protected int arg1;

    /**
     * Gets the value of the processInfoQuery property.
     * 
     * @return
     *     possible object is
     *     {@link TProcessInfoQuery }
     *     
     */
    public TProcessInfoQuery getProcessInfoQuery() {
        return processInfoQuery;
    }

    /**
     * Sets the value of the processInfoQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link TProcessInfoQuery }
     *     
     */
    public void setProcessInfoQuery(TProcessInfoQuery value) {
        this.processInfoQuery = value;
    }

    /**
     * Gets the value of the arg1 property.
     * 
     */
    public int getArg1() {
        return arg1;
    }

    /**
     * Sets the value of the arg1 property.
     * 
     */
    public void setArg1(int value) {
        this.arg1 = value;
    }

}
