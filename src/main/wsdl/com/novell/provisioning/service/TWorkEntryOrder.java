
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_workEntryOrder.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="t_workEntryOrder">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ADDRESSEE"/>
 *     &lt;enumeration value="PROCESS_ID"/>
 *     &lt;enumeration value="REQUEST_ID"/>
 *     &lt;enumeration value="ACTIVITY"/>
 *     &lt;enumeration value="OWNER"/>
 *     &lt;enumeration value="PRIORITY"/>
 *     &lt;enumeration value="CREATION"/>
 *     &lt;enumeration value="EXPIRATION"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "t_workEntryOrder")
@XmlEnum
public enum TWorkEntryOrder {

    ADDRESSEE,
    PROCESS_ID,
    REQUEST_ID,
    ACTIVITY,
    OWNER,
    PRIORITY,
    CREATION,
    EXPIRATION;

    public String value() {
        return name();
    }

    public static TWorkEntryOrder fromValue(String v) {
        return valueOf(v);
    }

}
