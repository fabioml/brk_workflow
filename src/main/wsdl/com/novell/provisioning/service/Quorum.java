
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Quorum complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Quorum">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="participantCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="approvalCondition" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="approveCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="denyCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="refuseCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Quorum", propOrder = {
    "participantCount",
    "approvalCondition",
    "approveCount",
    "denyCount",
    "refuseCount",
    "status"
})
public class Quorum {

    protected int participantCount;
    protected int approvalCondition;
    protected int approveCount;
    protected int denyCount;
    protected int refuseCount;
    protected int status;

    /**
     * Gets the value of the participantCount property.
     * 
     */
    public int getParticipantCount() {
        return participantCount;
    }

    /**
     * Sets the value of the participantCount property.
     * 
     */
    public void setParticipantCount(int value) {
        this.participantCount = value;
    }

    /**
     * Gets the value of the approvalCondition property.
     * 
     */
    public int getApprovalCondition() {
        return approvalCondition;
    }

    /**
     * Sets the value of the approvalCondition property.
     * 
     */
    public void setApprovalCondition(int value) {
        this.approvalCondition = value;
    }

    /**
     * Gets the value of the approveCount property.
     * 
     */
    public int getApproveCount() {
        return approveCount;
    }

    /**
     * Sets the value of the approveCount property.
     * 
     */
    public void setApproveCount(int value) {
        this.approveCount = value;
    }

    /**
     * Gets the value of the denyCount property.
     * 
     */
    public int getDenyCount() {
        return denyCount;
    }

    /**
     * Sets the value of the denyCount property.
     * 
     */
    public void setDenyCount(int value) {
        this.denyCount = value;
    }

    /**
     * Gets the value of the refuseCount property.
     * 
     */
    public int getRefuseCount() {
        return refuseCount;
    }

    /**
     * Sets the value of the refuseCount property.
     * 
     */
    public void setRefuseCount(int value) {
        this.refuseCount = value;
    }

    /**
     * Gets the value of the status property.
     * 
     */
    public int getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     */
    public void setStatus(int value) {
        this.status = value;
    }

}
