
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_processStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="t_processStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Running"/>
 *     &lt;enumeration value="Completed"/>
 *     &lt;enumeration value="Terminated"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "t_processStatus")
@XmlEnum
public enum TProcessStatus {

    @XmlEnumValue("Running")
    RUNNING("Running"),
    @XmlEnumValue("Completed")
    COMPLETED("Completed"),
    @XmlEnumValue("Terminated")
    TERMINATED("Terminated");
    private final String value;

    TProcessStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TProcessStatus fromValue(String v) {
        for (TProcessStatus c: TProcessStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
