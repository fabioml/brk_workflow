
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_engineStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="t_engineStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Starting"/>
 *     &lt;enumeration value="Running"/>
 *     &lt;enumeration value="Shutdown"/>
 *     &lt;enumeration value="Timedout"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "t_engineStatus")
@XmlEnum
public enum TEngineStatus {

    @XmlEnumValue("Starting")
    STARTING("Starting"),
    @XmlEnumValue("Running")
    RUNNING("Running"),
    @XmlEnumValue("Shutdown")
    SHUTDOWN("Shutdown"),
    @XmlEnumValue("Timedout")
    TIMEDOUT("Timedout");
    private final String value;

    TEngineStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TEngineStatus fromValue(String v) {
        for (TEngineStatus c: TEngineStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
