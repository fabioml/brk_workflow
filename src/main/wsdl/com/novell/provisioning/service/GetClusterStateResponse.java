
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getClusterStateResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getClusterStateResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.novell.com/provisioning/service}EngineStateArray"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getClusterStateResponse", propOrder = {
    "engineStateArray"
})
public class GetClusterStateResponse {

    @XmlElement(name = "EngineStateArray", required = true)
    protected EngineStateArray engineStateArray;

    /**
     * Gets the value of the engineStateArray property.
     * 
     * @return
     *     possible object is
     *     {@link EngineStateArray }
     *     
     */
    public EngineStateArray getEngineStateArray() {
        return engineStateArray;
    }

    /**
     * Sets the value of the engineStateArray property.
     * 
     * @param value
     *     allowed object is
     *     {@link EngineStateArray }
     *     
     */
    public void setEngineStateArray(EngineStateArray value) {
        this.engineStateArray = value;
    }

}
