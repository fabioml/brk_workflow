
package com.novell.provisioning.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.novell.provisioning.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TProvisioningStatusQueryState_QNAME = new QName("http://www.novell.com/provisioning/service", "state");
    private final static QName _TProvisioningStatusQueryResultTime_QNAME = new QName("http://www.novell.com/provisioning/service", "resultTime");
    private final static QName _TProvisioningStatusQueryRecipient_QNAME = new QName("http://www.novell.com/provisioning/service", "recipient");
    private final static QName _TProvisioningStatusQueryProvisioningTime_QNAME = new QName("http://www.novell.com/provisioning/service", "provisioningTime");
    private final static QName _TProvisioningStatusQueryActivityId_QNAME = new QName("http://www.novell.com/provisioning/service", "activityId");
    private final static QName _TProvisioningStatusQueryRequestId_QNAME = new QName("http://www.novell.com/provisioning/service", "requestId");
    private final static QName _TProvisioningStatusQueryStatus_QNAME = new QName("http://www.novell.com/provisioning/service", "status");
    private final static QName _GetAllProvisioningRequestsRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getAllProvisioningRequestsRequest");
    private final static QName _RemoveEngineRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "removeEngineRequest");
    private final static QName _ProvisioningStatusQuery_QNAME = new QName("http://www.novell.com/provisioning/service", "ProvisioningStatusQuery");
    private final static QName _GetClusterStateRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getClusterStateRequest");
    private final static QName _GetAllProcessesResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getAllProcessesResponse");
    private final static QName _ClearNIMCachesRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "clearNIMCachesRequest");
    private final static QName _GetProcessesArrayResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesArrayResponse");
    private final static QName _GetProvisioningRequestsRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getProvisioningRequestsRequest");
    private final static QName _StartAsProxyResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "startAsProxyResponse");
    private final static QName _Action_QNAME = new QName("http://www.novell.com/provisioning/service", "Action");
    private final static QName _GetProcessRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessRequest");
    private final static QName _StartResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "startResponse");
    private final static QName _TerminateRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "terminateRequest");
    private final static QName _AvailableActionArray_QNAME = new QName("http://www.novell.com/provisioning/service", "AvailableActionArray");
    private final static QName _GetCommentsByUserResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getCommentsByUserResponse");
    private final static QName _ReassignWorkTaskResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "reassignWorkTaskResponse");
    private final static QName _GetEmailNotificationsRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getEmailNotificationsRequest");
    private final static QName _ClearNIMCachesResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "clearNIMCachesResponse");
    private final static QName _SetWebServiceActivityTimeoutRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "setWebServiceActivityTimeoutRequest");
    private final static QName _GetCommentsRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getCommentsRequest");
    private final static QName _GetQuorumForWorkTaskRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getQuorumForWorkTaskRequest");
    private final static QName _ReassignProcessesResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "reassignProcessesResponse");
    private final static QName _String_QNAME = new QName("http://www.novell.com/provisioning/service", "string");
    private final static QName _GetEmailNotificationsResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getEmailNotificationsResponse");
    private final static QName _StartRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "startRequest");
    private final static QName _ReassignWorkTaskRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "reassignWorkTaskRequest");
    private final static QName _EngineStatus_QNAME = new QName("http://www.novell.com/provisioning/service", "EngineStatus");
    private final static QName _SetEngineConfigurationResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "setEngineConfigurationResponse");
    private final static QName _GetCompletedProcessTimeoutRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getCompletedProcessTimeoutRequest");
    private final static QName _GetEngineConfigurationResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getEngineConfigurationResponse");
    private final static QName _SetUserActivityTimeoutResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "setUserActivityTimeoutResponse");
    private final static QName _Boolean_QNAME = new QName("http://www.novell.com/provisioning/service", "boolean");
    private final static QName _GetProvisioningStatusesResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getProvisioningStatusesResponse");
    private final static QName _DataItemArray_QNAME = new QName("http://www.novell.com/provisioning/service", "DataItemArray");
    private final static QName _ResetPriorityForWorkTaskRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "resetPriorityForWorkTaskRequest");
    private final static QName _TerminateResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "terminateResponse");
    private final static QName _ReceiveRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "receiveRequest");
    private final static QName _Base64Binary_QNAME = new QName("http://www.novell.com/provisioning/service", "base64Binary");
    private final static QName _ReassignResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "reassignResponse");
    private final static QName _SetEmailNotificationsRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "setEmailNotificationsRequest");
    private final static QName _GetVersionRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getVersionRequest");
    private final static QName _CommentType_QNAME = new QName("http://www.novell.com/provisioning/service", "CommentType");
    private final static QName _GetClusterStateResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getClusterStateResponse");
    private final static QName _ReassignRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "reassignRequest");
    private final static QName _GetProcessesByRecipientRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByRecipientRequest");
    private final static QName _Float_QNAME = new QName("http://www.novell.com/provisioning/service", "float");
    private final static QName _ForwardResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "forwardResponse");
    private final static QName _GetProcessesByStatusRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByStatusRequest");
    private final static QName _Operator_QNAME = new QName("http://www.novell.com/provisioning/service", "Operator");
    private final static QName _EntitlementState_QNAME = new QName("http://www.novell.com/provisioning/service", "EntitlementState");
    private final static QName _GetCommentsByActivityResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getCommentsByActivityResponse");
    private final static QName _ResetPriorityForWorkTaskResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "resetPriorityForWorkTaskResponse");
    private final static QName _GetProcessesByCreationTimeRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByCreationTimeRequest");
    private final static QName _GetFormDefinitionRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getFormDefinitionRequest");
    private final static QName _ForwardAsProxyWithDigitalSignatureRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "forwardAsProxyWithDigitalSignatureRequest");
    private final static QName _GetProcessesByIdResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByIdResponse");
    private final static QName _StartWithCorrelationIdResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "startWithCorrelationIdResponse");
    private final static QName _GetProcessesByCreationTimeResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByCreationTimeResponse");
    private final static QName _GetProcessesByQueryResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByQueryResponse");
    private final static QName _ForwardWithDigitalSignatureRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "forwardWithDigitalSignatureRequest");
    private final static QName _GetProcessesResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesResponse");
    private final static QName _GetCompletedProcessTimeoutResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getCompletedProcessTimeoutResponse");
    private final static QName _AvailableAction_QNAME = new QName("http://www.novell.com/provisioning/service", "AvailableAction");
    private final static QName _GetWebServiceActivityTimeoutRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getWebServiceActivityTimeoutRequest");
    private final static QName _Comment_QNAME = new QName("http://www.novell.com/provisioning/service", "Comment");
    private final static QName _StartWithDigitalSignatureResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "startWithDigitalSignatureResponse");
    private final static QName _GetEngineStateResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getEngineStateResponse");
    private final static QName _DataItem_QNAME = new QName("http://www.novell.com/provisioning/service", "DataItem");
    private final static QName _GetProvisioningRequestsResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getProvisioningRequestsResponse");
    private final static QName _SetWebServiceActivityTimeoutResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "setWebServiceActivityTimeoutResponse");
    private final static QName _GetCommentsByTypeResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getCommentsByTypeResponse");
    private final static QName _ForwardWithDigitalSignatureResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "forwardWithDigitalSignatureResponse");
    private final static QName _StartAsProxyWithDigitalSignatureResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "startAsProxyWithDigitalSignatureResponse");
    private final static QName _ProcessArray_QNAME = new QName("http://www.novell.com/provisioning/service", "ProcessArray");
    private final static QName _UnclaimResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "unclaimResponse");
    private final static QName _SetEmailNotificationsResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "setEmailNotificationsResponse");
    private final static QName _ForwardAsProxyResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "forwardAsProxyResponse");
    private final static QName _CommentArray_QNAME = new QName("http://www.novell.com/provisioning/service", "CommentArray");
    private final static QName _GetUserActivityTimeoutResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getUserActivityTimeoutResponse");
    private final static QName _ReceiveResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "receiveResponse");
    private final static QName _MultiStartRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "multiStartRequest");
    private final static QName _ProvisioningStatus_QNAME = new QName("http://www.novell.com/provisioning/service", "ProvisioningStatus");
    private final static QName _GetProcessesByStatusResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByStatusResponse");
    private final static QName _StringArray_QNAME = new QName("http://www.novell.com/provisioning/service", "StringArray");
    private final static QName _GetProcessesByRecipientResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByRecipientResponse");
    private final static QName _SetResultResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "setResultResponse");
    private final static QName _ReassignProcessesRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "reassignProcessesRequest");
    private final static QName _GetEngineConfigurationRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getEngineConfigurationRequest");
    private final static QName _SignaturePropertyArray_QNAME = new QName("http://www.novell.com/provisioning/service", "SignaturePropertyArray");
    private final static QName _MultiStartResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "multiStartResponse");
    private final static QName _AdminException_QNAME = new QName("http://www.novell.com/provisioning/service", "AdminException");
    private final static QName _GetCommentsResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getCommentsResponse");
    private final static QName _ForwardRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "forwardRequest");
    private final static QName _ReassignAllProcessesResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "reassignAllProcessesResponse");
    private final static QName _SetResultRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "setResultRequest");
    private final static QName _DateTime_QNAME = new QName("http://www.novell.com/provisioning/service", "dateTime");
    private final static QName _GetCommentsByCreationTimeRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getCommentsByCreationTimeRequest");
    private final static QName _AddCommentRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "addCommentRequest");
    private final static QName _ApprovalStatus_QNAME = new QName("http://www.novell.com/provisioning/service", "ApprovalStatus");
    private final static QName _GetProcessesByInitiatorRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByInitiatorRequest");
    private final static QName _ProvisioningRequestArray_QNAME = new QName("http://www.novell.com/provisioning/service", "ProvisioningRequestArray");
    private final static QName _TerminationType_QNAME = new QName("http://www.novell.com/provisioning/service", "TerminationType");
    private final static QName _GetWorkResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getWorkResponse");
    private final static QName _GetCommentsByCreationTimeResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getCommentsByCreationTimeResponse");
    private final static QName _EntitlementStatus_QNAME = new QName("http://www.novell.com/provisioning/service", "EntitlementStatus");
    private final static QName _ReassignPercentageProcessesRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "reassignPercentageProcessesRequest");
    private final static QName _Quorum_QNAME = new QName("http://www.novell.com/provisioning/service", "Quorum");
    private final static QName _SetUserActivityTimeoutRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "setUserActivityTimeoutRequest");
    private final static QName _GetProcessesByCreationIntervalResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByCreationIntervalResponse");
    private final static QName _ProcessInfoQuery_QNAME = new QName("http://www.novell.com/provisioning/service", "ProcessInfoQuery");
    private final static QName _RemoveEngineResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "removeEngineResponse");
    private final static QName _GetProcessesRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesRequest");
    private final static QName _Configuration_QNAME = new QName("http://www.novell.com/provisioning/service", "Configuration");
    private final static QName _SetEngineConfigurationRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "setEngineConfigurationRequest");
    private final static QName _GetProcessesByIdRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByIdRequest");
    private final static QName _GetProcessesArrayRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesArrayRequest");
    private final static QName _GetFlowDefinitionResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getFlowDefinitionResponse");
    private final static QName _GetCommentsByTypeRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getCommentsByTypeRequest");
    private final static QName _StartAsProxyRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "startAsProxyRequest");
    private final static QName _GetEngineStateRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getEngineStateRequest");
    private final static QName _EngineState_QNAME = new QName("http://www.novell.com/provisioning/service", "EngineState");
    private final static QName _GetProcessesByCreationIntervalRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByCreationIntervalRequest");
    private final static QName _GetGraphResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getGraphResponse");
    private final static QName _GetUserActivityTimeoutRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getUserActivityTimeoutRequest");
    private final static QName _ProcessStatus_QNAME = new QName("http://www.novell.com/provisioning/service", "ProcessStatus");
    private final static QName _ReassignPercentageProcessesResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "reassignPercentageProcessesResponse");
    private final static QName _GetFlowDefinitionRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getFlowDefinitionRequest");
    private final static QName _Version_QNAME = new QName("http://www.novell.com/provisioning/service", "Version");
    private final static QName _GetCommentsByActivityRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getCommentsByActivityRequest");
    private final static QName _EngineStateArray_QNAME = new QName("http://www.novell.com/provisioning/service", "EngineStateArray");
    private final static QName _Process_QNAME = new QName("http://www.novell.com/provisioning/service", "Process");
    private final static QName _GetProcessesByApprovalStatusResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByApprovalStatusResponse");
    private final static QName _SignatureProperty_QNAME = new QName("http://www.novell.com/provisioning/service", "SignatureProperty");
    private final static QName _StartAsProxyWithDigitalSignatureRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "startAsProxyWithDigitalSignatureRequest");
    private final static QName _GetWorkEntriesRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getWorkEntriesRequest");
    private final static QName _GetGraphRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getGraphRequest");
    private final static QName _Long_QNAME = new QName("http://www.novell.com/provisioning/service", "long");
    private final static QName _GetVersionResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getVersionResponse");
    private final static QName _GetProvisioningStatusesRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getProvisioningStatusesRequest");
    private final static QName _StartWithCorrelationIdRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "startWithCorrelationIdRequest");
    private final static QName _WorkEntryQuery_QNAME = new QName("http://www.novell.com/provisioning/service", "WorkEntryQuery");
    private final static QName _ProvisioningRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "ProvisioningRequest");
    private final static QName _SetRoleRequestStatusResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "setRoleRequestStatusResponse");
    private final static QName _WorkEntry_QNAME = new QName("http://www.novell.com/provisioning/service", "WorkEntry");
    private final static QName _AddCommentResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "addCommentResponse");
    private final static QName _GetWorkRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getWorkRequest");
    private final static QName _ForwardAsProxyRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "forwardAsProxyRequest");
    private final static QName _ProvisioningStatusArray_QNAME = new QName("http://www.novell.com/provisioning/service", "ProvisioningStatusArray");
    private final static QName _GetProcessesByInitiatorResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByInitiatorResponse");
    private final static QName _SetRoleRequestStatusRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "setRoleRequestStatusRequest");
    private final static QName _UnclaimRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "unclaimRequest");
    private final static QName _GetAllProcessesRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getAllProcessesRequest");
    private final static QName _GetAllProvisioningRequestsResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getAllProvisioningRequestsResponse");
    private final static QName _GetWebServiceActivityTimeoutResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getWebServiceActivityTimeoutResponse");
    private final static QName _GetProvisioningCategoriesRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getProvisioningCategoriesRequest");
    private final static QName _GetProcessesByApprovalStatusRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByApprovalStatusRequest");
    private final static QName _GetProcessResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessResponse");
    private final static QName _GetCommentsByUserRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getCommentsByUserRequest");
    private final static QName _GetProcessesByQueryRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "getProcessesByQueryRequest");
    private final static QName _SetCompletedProcessTimeoutRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "setCompletedProcessTimeoutRequest");
    private final static QName _WorkEntryArray_QNAME = new QName("http://www.novell.com/provisioning/service", "WorkEntryArray");
    private final static QName _GetQuorumForWorkTaskResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getQuorumForWorkTaskResponse");
    private final static QName _GetWorkEntriesResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getWorkEntriesResponse");
    private final static QName _GetFormDefinitionResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getFormDefinitionResponse");
    private final static QName _GetProvisioningCategoriesResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "getProvisioningCategoriesResponse");
    private final static QName _StartWithDigitalSignatureRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "startWithDigitalSignatureRequest");
    private final static QName _ForwardAsProxyWithDigitalSignatureResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "forwardAsProxyWithDigitalSignatureResponse");
    private final static QName _ReassignAllProcessesRequest_QNAME = new QName("http://www.novell.com/provisioning/service", "reassignAllProcessesRequest");
    private final static QName _Int_QNAME = new QName("http://www.novell.com/provisioning/service", "int");
    private final static QName _SetCompletedProcessTimeoutResponse_QNAME = new QName("http://www.novell.com/provisioning/service", "setCompletedProcessTimeoutResponse");
    private final static QName _TProcessInfoQueryEngineId_QNAME = new QName("http://www.novell.com/provisioning/service", "engineId");
    private final static QName _TProcessInfoQueryCreationTime_QNAME = new QName("http://www.novell.com/provisioning/service", "creationTime");
    private final static QName _TProcessInfoQueryResourceType_QNAME = new QName("http://www.novell.com/provisioning/service", "resourceType");
    private final static QName _TProcessInfoQueryCompletionTime_QNAME = new QName("http://www.novell.com/provisioning/service", "completionTime");
    private final static QName _TProcessInfoQueryCorrelationId_QNAME = new QName("http://www.novell.com/provisioning/service", "correlationId");
    private final static QName _TProcessInfoQueryApprovalStatus_QNAME = new QName("http://www.novell.com/provisioning/service", "approvalStatus");
    private final static QName _TProcessInfoQueryProcessStatus_QNAME = new QName("http://www.novell.com/provisioning/service", "processStatus");
    private final static QName _TProcessInfoQueryProcessId_QNAME = new QName("http://www.novell.com/provisioning/service", "processId");
    private final static QName _TProcessInfoQueryInitiator_QNAME = new QName("http://www.novell.com/provisioning/service", "initiator");
    private final static QName _TWorkEntryQueryAddressee_QNAME = new QName("http://www.novell.com/provisioning/service", "addressee");
    private final static QName _TWorkEntryQueryOwner_QNAME = new QName("http://www.novell.com/provisioning/service", "owner");
    private final static QName _TWorkEntryQueryProxyFor_QNAME = new QName("http://www.novell.com/provisioning/service", "proxyFor");
    private final static QName _TWorkEntryQueryExpTime_QNAME = new QName("http://www.novell.com/provisioning/service", "expTime");
    private final static QName _TWorkEntryQueryPriority_QNAME = new QName("http://www.novell.com/provisioning/service", "priority");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.novell.provisioning.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ResetPriorityForWorkTaskResponse }
     * 
     */
    public ResetPriorityForWorkTaskResponse createResetPriorityForWorkTaskResponse() {
        return new ResetPriorityForWorkTaskResponse();
    }

    /**
     * Create an instance of {@link GetProvisioningCategoriesRequest }
     * 
     */
    public GetProvisioningCategoriesRequest createGetProvisioningCategoriesRequest() {
        return new GetProvisioningCategoriesRequest();
    }

    /**
     * Create an instance of {@link GetProcessesByInitiatorRequest }
     * 
     */
    public GetProcessesByInitiatorRequest createGetProcessesByInitiatorRequest() {
        return new GetProcessesByInitiatorRequest();
    }

    /**
     * Create an instance of {@link GetWebServiceActivityTimeoutRequest }
     * 
     */
    public GetWebServiceActivityTimeoutRequest createGetWebServiceActivityTimeoutRequest() {
        return new GetWebServiceActivityTimeoutRequest();
    }

    /**
     * Create an instance of {@link GetProvisioningStatusesResponse }
     * 
     */
    public GetProvisioningStatusesResponse createGetProvisioningStatusesResponse() {
        return new GetProvisioningStatusesResponse();
    }

    /**
     * Create an instance of {@link GetAllProvisioningRequestsRequest }
     * 
     */
    public GetAllProvisioningRequestsRequest createGetAllProvisioningRequestsRequest() {
        return new GetAllProvisioningRequestsRequest();
    }

    /**
     * Create an instance of {@link GetWorkRequest }
     * 
     */
    public GetWorkRequest createGetWorkRequest() {
        return new GetWorkRequest();
    }

    /**
     * Create an instance of {@link GetProcessesRequest }
     * 
     */
    public GetProcessesRequest createGetProcessesRequest() {
        return new GetProcessesRequest();
    }

    /**
     * Create an instance of {@link GetCommentsByTypeRequest }
     * 
     */
    public GetCommentsByTypeRequest createGetCommentsByTypeRequest() {
        return new GetCommentsByTypeRequest();
    }

    /**
     * Create an instance of {@link GetAllProcessesRequest }
     * 
     */
    public GetAllProcessesRequest createGetAllProcessesRequest() {
        return new GetAllProcessesRequest();
    }

    /**
     * Create an instance of {@link ForwardWithDigitalSignatureResponse }
     * 
     */
    public ForwardWithDigitalSignatureResponse createForwardWithDigitalSignatureResponse() {
        return new ForwardWithDigitalSignatureResponse();
    }

    /**
     * Create an instance of {@link GetProcessesByQueryResponse }
     * 
     */
    public GetProcessesByQueryResponse createGetProcessesByQueryResponse() {
        return new GetProcessesByQueryResponse();
    }

    /**
     * Create an instance of {@link ReassignAllProcessesResponse }
     * 
     */
    public ReassignAllProcessesResponse createReassignAllProcessesResponse() {
        return new ReassignAllProcessesResponse();
    }

    /**
     * Create an instance of {@link TProcessInfoQuery }
     * 
     */
    public TProcessInfoQuery createTProcessInfoQuery() {
        return new TProcessInfoQuery();
    }

    /**
     * Create an instance of {@link GetProcessesResponse }
     * 
     */
    public GetProcessesResponse createGetProcessesResponse() {
        return new GetProcessesResponse();
    }

    /**
     * Create an instance of {@link ReassignPercentageProcessesResponse }
     * 
     */
    public ReassignPercentageProcessesResponse createReassignPercentageProcessesResponse() {
        return new ReassignPercentageProcessesResponse();
    }

    /**
     * Create an instance of {@link AddCommentResponse }
     * 
     */
    public AddCommentResponse createAddCommentResponse() {
        return new AddCommentResponse();
    }

    /**
     * Create an instance of {@link GetProcessesByRecipientResponse }
     * 
     */
    public GetProcessesByRecipientResponse createGetProcessesByRecipientResponse() {
        return new GetProcessesByRecipientResponse();
    }

    /**
     * Create an instance of {@link GetUserActivityTimeoutRequest }
     * 
     */
    public GetUserActivityTimeoutRequest createGetUserActivityTimeoutRequest() {
        return new GetUserActivityTimeoutRequest();
    }

    /**
     * Create an instance of {@link GetQuorumForWorkTaskRequest }
     * 
     */
    public GetQuorumForWorkTaskRequest createGetQuorumForWorkTaskRequest() {
        return new GetQuorumForWorkTaskRequest();
    }

    /**
     * Create an instance of {@link ProvisioningStatus }
     * 
     */
    public ProvisioningStatus createProvisioningStatus() {
        return new ProvisioningStatus();
    }

    /**
     * Create an instance of {@link Process }
     * 
     */
    public Process createProcess() {
        return new Process();
    }

    /**
     * Create an instance of {@link GetWorkEntriesRequest }
     * 
     */
    public GetWorkEntriesRequest createGetWorkEntriesRequest() {
        return new GetWorkEntriesRequest();
    }

    /**
     * Create an instance of {@link ProvisioningRequestArray }
     * 
     */
    public ProvisioningRequestArray createProvisioningRequestArray() {
        return new ProvisioningRequestArray();
    }

    /**
     * Create an instance of {@link GetFormDefinitionResponse }
     * 
     */
    public GetFormDefinitionResponse createGetFormDefinitionResponse() {
        return new GetFormDefinitionResponse();
    }

    /**
     * Create an instance of {@link GetProcessesByRecipientRequest }
     * 
     */
    public GetProcessesByRecipientRequest createGetProcessesByRecipientRequest() {
        return new GetProcessesByRecipientRequest();
    }

    /**
     * Create an instance of {@link ReceiveResponse }
     * 
     */
    public ReceiveResponse createReceiveResponse() {
        return new ReceiveResponse();
    }

    /**
     * Create an instance of {@link GetWorkEntriesResponse }
     * 
     */
    public GetWorkEntriesResponse createGetWorkEntriesResponse() {
        return new GetWorkEntriesResponse();
    }

    /**
     * Create an instance of {@link Configuration }
     * 
     */
    public Configuration createConfiguration() {
        return new Configuration();
    }

    /**
     * Create an instance of {@link GetEngineConfigurationRequest }
     * 
     */
    public GetEngineConfigurationRequest createGetEngineConfigurationRequest() {
        return new GetEngineConfigurationRequest();
    }

    /**
     * Create an instance of {@link SetUserActivityTimeoutResponse }
     * 
     */
    public SetUserActivityTimeoutResponse createSetUserActivityTimeoutResponse() {
        return new SetUserActivityTimeoutResponse();
    }

    /**
     * Create an instance of {@link GetProcessesArrayResponse }
     * 
     */
    public GetProcessesArrayResponse createGetProcessesArrayResponse() {
        return new GetProcessesArrayResponse();
    }

    /**
     * Create an instance of {@link ReassignProcessesResponse }
     * 
     */
    public ReassignProcessesResponse createReassignProcessesResponse() {
        return new ReassignProcessesResponse();
    }

    /**
     * Create an instance of {@link GetProcessesByStatusResponse }
     * 
     */
    public GetProcessesByStatusResponse createGetProcessesByStatusResponse() {
        return new GetProcessesByStatusResponse();
    }

    /**
     * Create an instance of {@link GetProcessesByCreationIntervalRequest }
     * 
     */
    public GetProcessesByCreationIntervalRequest createGetProcessesByCreationIntervalRequest() {
        return new GetProcessesByCreationIntervalRequest();
    }

    /**
     * Create an instance of {@link GetProvisioningCategoriesResponse }
     * 
     */
    public GetProvisioningCategoriesResponse createGetProvisioningCategoriesResponse() {
        return new GetProvisioningCategoriesResponse();
    }

    /**
     * Create an instance of {@link GetProcessesArrayRequest }
     * 
     */
    public GetProcessesArrayRequest createGetProcessesArrayRequest() {
        return new GetProcessesArrayRequest();
    }

    /**
     * Create an instance of {@link StartWithCorrelationIdResponse }
     * 
     */
    public StartWithCorrelationIdResponse createStartWithCorrelationIdResponse() {
        return new StartWithCorrelationIdResponse();
    }

    /**
     * Create an instance of {@link EngineStateArray }
     * 
     */
    public EngineStateArray createEngineStateArray() {
        return new EngineStateArray();
    }

    /**
     * Create an instance of {@link ForwardRequest }
     * 
     */
    public ForwardRequest createForwardRequest() {
        return new ForwardRequest();
    }

    /**
     * Create an instance of {@link GetCommentsByUserResponse }
     * 
     */
    public GetCommentsByUserResponse createGetCommentsByUserResponse() {
        return new GetCommentsByUserResponse();
    }

    /**
     * Create an instance of {@link ClearNIMCachesRequest }
     * 
     */
    public ClearNIMCachesRequest createClearNIMCachesRequest() {
        return new ClearNIMCachesRequest();
    }

    /**
     * Create an instance of {@link GetEngineConfigurationResponse }
     * 
     */
    public GetEngineConfigurationResponse createGetEngineConfigurationResponse() {
        return new GetEngineConfigurationResponse();
    }

    /**
     * Create an instance of {@link GetCommentsRequest }
     * 
     */
    public GetCommentsRequest createGetCommentsRequest() {
        return new GetCommentsRequest();
    }

    /**
     * Create an instance of {@link WorkEntryArray }
     * 
     */
    public WorkEntryArray createWorkEntryArray() {
        return new WorkEntryArray();
    }

    /**
     * Create an instance of {@link ReassignRequest }
     * 
     */
    public ReassignRequest createReassignRequest() {
        return new ReassignRequest();
    }

    /**
     * Create an instance of {@link EngineState }
     * 
     */
    public EngineState createEngineState() {
        return new EngineState();
    }

    /**
     * Create an instance of {@link SetCompletedProcessTimeoutResponse }
     * 
     */
    public SetCompletedProcessTimeoutResponse createSetCompletedProcessTimeoutResponse() {
        return new SetCompletedProcessTimeoutResponse();
    }

    /**
     * Create an instance of {@link ForwardResponse }
     * 
     */
    public ForwardResponse createForwardResponse() {
        return new ForwardResponse();
    }

    /**
     * Create an instance of {@link GetFlowDefinitionRequest }
     * 
     */
    public GetFlowDefinitionRequest createGetFlowDefinitionRequest() {
        return new GetFlowDefinitionRequest();
    }

    /**
     * Create an instance of {@link TerminateResponse }
     * 
     */
    public TerminateResponse createTerminateResponse() {
        return new TerminateResponse();
    }

    /**
     * Create an instance of {@link SetWebServiceActivityTimeoutResponse }
     * 
     */
    public SetWebServiceActivityTimeoutResponse createSetWebServiceActivityTimeoutResponse() {
        return new SetWebServiceActivityTimeoutResponse();
    }

    /**
     * Create an instance of {@link GetEmailNotificationsRequest }
     * 
     */
    public GetEmailNotificationsRequest createGetEmailNotificationsRequest() {
        return new GetEmailNotificationsRequest();
    }

    /**
     * Create an instance of {@link GetProcessesByIdResponse }
     * 
     */
    public GetProcessesByIdResponse createGetProcessesByIdResponse() {
        return new GetProcessesByIdResponse();
    }

    /**
     * Create an instance of {@link GetCommentsByActivityRequest }
     * 
     */
    public GetCommentsByActivityRequest createGetCommentsByActivityRequest() {
        return new GetCommentsByActivityRequest();
    }

    /**
     * Create an instance of {@link GetProcessesByApprovalStatusRequest }
     * 
     */
    public GetProcessesByApprovalStatusRequest createGetProcessesByApprovalStatusRequest() {
        return new GetProcessesByApprovalStatusRequest();
    }

    /**
     * Create an instance of {@link GetProcessResponse }
     * 
     */
    public GetProcessResponse createGetProcessResponse() {
        return new GetProcessResponse();
    }

    /**
     * Create an instance of {@link GetAllProcessesResponse }
     * 
     */
    public GetAllProcessesResponse createGetAllProcessesResponse() {
        return new GetAllProcessesResponse();
    }

    /**
     * Create an instance of {@link TProvisioningStatusQuery }
     * 
     */
    public TProvisioningStatusQuery createTProvisioningStatusQuery() {
        return new TProvisioningStatusQuery();
    }

    /**
     * Create an instance of {@link GetGraphResponse }
     * 
     */
    public GetGraphResponse createGetGraphResponse() {
        return new GetGraphResponse();
    }

    /**
     * Create an instance of {@link GetClusterStateResponse }
     * 
     */
    public GetClusterStateResponse createGetClusterStateResponse() {
        return new GetClusterStateResponse();
    }

    /**
     * Create an instance of {@link GetFormDefinitionRequest }
     * 
     */
    public GetFormDefinitionRequest createGetFormDefinitionRequest() {
        return new GetFormDefinitionRequest();
    }

    /**
     * Create an instance of {@link AvailableAction }
     * 
     */
    public AvailableAction createAvailableAction() {
        return new AvailableAction();
    }

    /**
     * Create an instance of {@link SetEmailNotificationsRequest }
     * 
     */
    public SetEmailNotificationsRequest createSetEmailNotificationsRequest() {
        return new SetEmailNotificationsRequest();
    }

    /**
     * Create an instance of {@link GetFlowDefinitionResponse }
     * 
     */
    public GetFlowDefinitionResponse createGetFlowDefinitionResponse() {
        return new GetFlowDefinitionResponse();
    }

    /**
     * Create an instance of {@link ReassignPercentageProcessesRequest }
     * 
     */
    public ReassignPercentageProcessesRequest createReassignPercentageProcessesRequest() {
        return new ReassignPercentageProcessesRequest();
    }

    /**
     * Create an instance of {@link SetResultResponse }
     * 
     */
    public SetResultResponse createSetResultResponse() {
        return new SetResultResponse();
    }

    /**
     * Create an instance of {@link StartWithDigitalSignatureResponse }
     * 
     */
    public StartWithDigitalSignatureResponse createStartWithDigitalSignatureResponse() {
        return new StartWithDigitalSignatureResponse();
    }

    /**
     * Create an instance of {@link GetProcessesByIdRequest }
     * 
     */
    public GetProcessesByIdRequest createGetProcessesByIdRequest() {
        return new GetProcessesByIdRequest();
    }

    /**
     * Create an instance of {@link ReceiveRequest }
     * 
     */
    public ReceiveRequest createReceiveRequest() {
        return new ReceiveRequest();
    }

    /**
     * Create an instance of {@link TWorkEntryQuery }
     * 
     */
    public TWorkEntryQuery createTWorkEntryQuery() {
        return new TWorkEntryQuery();
    }

    /**
     * Create an instance of {@link GetVersionRequest }
     * 
     */
    public GetVersionRequest createGetVersionRequest() {
        return new GetVersionRequest();
    }

    /**
     * Create an instance of {@link SignatureProperty }
     * 
     */
    public SignatureProperty createSignatureProperty() {
        return new SignatureProperty();
    }

    /**
     * Create an instance of {@link ReassignWorkTaskRequest }
     * 
     */
    public ReassignWorkTaskRequest createReassignWorkTaskRequest() {
        return new ReassignWorkTaskRequest();
    }

    /**
     * Create an instance of {@link GetCommentsByCreationTimeResponse }
     * 
     */
    public GetCommentsByCreationTimeResponse createGetCommentsByCreationTimeResponse() {
        return new GetCommentsByCreationTimeResponse();
    }

    /**
     * Create an instance of {@link SetCompletedProcessTimeoutRequest }
     * 
     */
    public SetCompletedProcessTimeoutRequest createSetCompletedProcessTimeoutRequest() {
        return new SetCompletedProcessTimeoutRequest();
    }

    /**
     * Create an instance of {@link ForwardAsProxyResponse }
     * 
     */
    public ForwardAsProxyResponse createForwardAsProxyResponse() {
        return new ForwardAsProxyResponse();
    }

    /**
     * Create an instance of {@link GetProcessesByInitiatorResponse }
     * 
     */
    public GetProcessesByInitiatorResponse createGetProcessesByInitiatorResponse() {
        return new GetProcessesByInitiatorResponse();
    }

    /**
     * Create an instance of {@link UnclaimResponse }
     * 
     */
    public UnclaimResponse createUnclaimResponse() {
        return new UnclaimResponse();
    }

    /**
     * Create an instance of {@link SetWebServiceActivityTimeoutRequest }
     * 
     */
    public SetWebServiceActivityTimeoutRequest createSetWebServiceActivityTimeoutRequest() {
        return new SetWebServiceActivityTimeoutRequest();
    }

    /**
     * Create an instance of {@link StartWithCorrelationIdRequest }
     * 
     */
    public StartWithCorrelationIdRequest createStartWithCorrelationIdRequest() {
        return new StartWithCorrelationIdRequest();
    }

    /**
     * Create an instance of {@link GetWorkResponse }
     * 
     */
    public GetWorkResponse createGetWorkResponse() {
        return new GetWorkResponse();
    }

    /**
     * Create an instance of {@link GetCommentsByActivityResponse }
     * 
     */
    public GetCommentsByActivityResponse createGetCommentsByActivityResponse() {
        return new GetCommentsByActivityResponse();
    }

    /**
     * Create an instance of {@link ForwardAsProxyWithDigitalSignatureRequest }
     * 
     */
    public ForwardAsProxyWithDigitalSignatureRequest createForwardAsProxyWithDigitalSignatureRequest() {
        return new ForwardAsProxyWithDigitalSignatureRequest();
    }

    /**
     * Create an instance of {@link AvailableActionArray }
     * 
     */
    public AvailableActionArray createAvailableActionArray() {
        return new AvailableActionArray();
    }

    /**
     * Create an instance of {@link ReassignResponse }
     * 
     */
    public ReassignResponse createReassignResponse() {
        return new ReassignResponse();
    }

    /**
     * Create an instance of {@link GetQuorumForWorkTaskResponse }
     * 
     */
    public GetQuorumForWorkTaskResponse createGetQuorumForWorkTaskResponse() {
        return new GetQuorumForWorkTaskResponse();
    }

    /**
     * Create an instance of {@link GetProcessRequest }
     * 
     */
    public GetProcessRequest createGetProcessRequest() {
        return new GetProcessRequest();
    }

    /**
     * Create an instance of {@link SetEngineConfigurationRequest }
     * 
     */
    public SetEngineConfigurationRequest createSetEngineConfigurationRequest() {
        return new SetEngineConfigurationRequest();
    }

    /**
     * Create an instance of {@link GetProcessesByQueryRequest }
     * 
     */
    public GetProcessesByQueryRequest createGetProcessesByQueryRequest() {
        return new GetProcessesByQueryRequest();
    }

    /**
     * Create an instance of {@link AdminException }
     * 
     */
    public AdminException createAdminException() {
        return new AdminException();
    }

    /**
     * Create an instance of {@link StartAsProxyWithDigitalSignatureResponse }
     * 
     */
    public StartAsProxyWithDigitalSignatureResponse createStartAsProxyWithDigitalSignatureResponse() {
        return new StartAsProxyWithDigitalSignatureResponse();
    }

    /**
     * Create an instance of {@link ReassignProcessesRequest }
     * 
     */
    public ReassignProcessesRequest createReassignProcessesRequest() {
        return new ReassignProcessesRequest();
    }

    /**
     * Create an instance of {@link GetCompletedProcessTimeoutRequest }
     * 
     */
    public GetCompletedProcessTimeoutRequest createGetCompletedProcessTimeoutRequest() {
        return new GetCompletedProcessTimeoutRequest();
    }

    /**
     * Create an instance of {@link StartWithDigitalSignatureRequest }
     * 
     */
    public StartWithDigitalSignatureRequest createStartWithDigitalSignatureRequest() {
        return new StartWithDigitalSignatureRequest();
    }

    /**
     * Create an instance of {@link GetEngineStateResponse }
     * 
     */
    public GetEngineStateResponse createGetEngineStateResponse() {
        return new GetEngineStateResponse();
    }

    /**
     * Create an instance of {@link StartResponse }
     * 
     */
    public StartResponse createStartResponse() {
        return new StartResponse();
    }

    /**
     * Create an instance of {@link RemoveEngineRequest }
     * 
     */
    public RemoveEngineRequest createRemoveEngineRequest() {
        return new RemoveEngineRequest();
    }

    /**
     * Create an instance of {@link ResetPriorityForWorkTaskRequest }
     * 
     */
    public ResetPriorityForWorkTaskRequest createResetPriorityForWorkTaskRequest() {
        return new ResetPriorityForWorkTaskRequest();
    }

    /**
     * Create an instance of {@link DataItemArray }
     * 
     */
    public DataItemArray createDataItemArray() {
        return new DataItemArray();
    }

    /**
     * Create an instance of {@link Quorum }
     * 
     */
    public Quorum createQuorum() {
        return new Quorum();
    }

    /**
     * Create an instance of {@link ProcessArray }
     * 
     */
    public ProcessArray createProcessArray() {
        return new ProcessArray();
    }

    /**
     * Create an instance of {@link SetEmailNotificationsResponse }
     * 
     */
    public SetEmailNotificationsResponse createSetEmailNotificationsResponse() {
        return new SetEmailNotificationsResponse();
    }

    /**
     * Create an instance of {@link MultiStartRequest }
     * 
     */
    public MultiStartRequest createMultiStartRequest() {
        return new MultiStartRequest();
    }

    /**
     * Create an instance of {@link GetWebServiceActivityTimeoutResponse }
     * 
     */
    public GetWebServiceActivityTimeoutResponse createGetWebServiceActivityTimeoutResponse() {
        return new GetWebServiceActivityTimeoutResponse();
    }

    /**
     * Create an instance of {@link UnclaimRequest }
     * 
     */
    public UnclaimRequest createUnclaimRequest() {
        return new UnclaimRequest();
    }

    /**
     * Create an instance of {@link GetProcessesByCreationTimeResponse }
     * 
     */
    public GetProcessesByCreationTimeResponse createGetProcessesByCreationTimeResponse() {
        return new GetProcessesByCreationTimeResponse();
    }

    /**
     * Create an instance of {@link StartAsProxyWithDigitalSignatureRequest }
     * 
     */
    public StartAsProxyWithDigitalSignatureRequest createStartAsProxyWithDigitalSignatureRequest() {
        return new StartAsProxyWithDigitalSignatureRequest();
    }

    /**
     * Create an instance of {@link TVersion }
     * 
     */
    public TVersion createTVersion() {
        return new TVersion();
    }

    /**
     * Create an instance of {@link ForwardAsProxyRequest }
     * 
     */
    public ForwardAsProxyRequest createForwardAsProxyRequest() {
        return new ForwardAsProxyRequest();
    }

    /**
     * Create an instance of {@link GetCommentsByUserRequest }
     * 
     */
    public GetCommentsByUserRequest createGetCommentsByUserRequest() {
        return new GetCommentsByUserRequest();
    }

    /**
     * Create an instance of {@link DataItem }
     * 
     */
    public DataItem createDataItem() {
        return new DataItem();
    }

    /**
     * Create an instance of {@link StartRequest }
     * 
     */
    public StartRequest createStartRequest() {
        return new StartRequest();
    }

    /**
     * Create an instance of {@link GetVersionResponse }
     * 
     */
    public GetVersionResponse createGetVersionResponse() {
        return new GetVersionResponse();
    }

    /**
     * Create an instance of {@link GetCompletedProcessTimeoutResponse }
     * 
     */
    public GetCompletedProcessTimeoutResponse createGetCompletedProcessTimeoutResponse() {
        return new GetCompletedProcessTimeoutResponse();
    }

    /**
     * Create an instance of {@link ReassignAllProcessesRequest }
     * 
     */
    public ReassignAllProcessesRequest createReassignAllProcessesRequest() {
        return new ReassignAllProcessesRequest();
    }

    /**
     * Create an instance of {@link ForwardWithDigitalSignatureRequest }
     * 
     */
    public ForwardWithDigitalSignatureRequest createForwardWithDigitalSignatureRequest() {
        return new ForwardWithDigitalSignatureRequest();
    }

    /**
     * Create an instance of {@link GetProvisioningRequestsRequest }
     * 
     */
    public GetProvisioningRequestsRequest createGetProvisioningRequestsRequest() {
        return new GetProvisioningRequestsRequest();
    }

    /**
     * Create an instance of {@link RemoveEngineResponse }
     * 
     */
    public RemoveEngineResponse createRemoveEngineResponse() {
        return new RemoveEngineResponse();
    }

    /**
     * Create an instance of {@link GetProvisioningRequestsResponse }
     * 
     */
    public GetProvisioningRequestsResponse createGetProvisioningRequestsResponse() {
        return new GetProvisioningRequestsResponse();
    }

    /**
     * Create an instance of {@link TTime }
     * 
     */
    public TTime createTTime() {
        return new TTime();
    }

    /**
     * Create an instance of {@link GetCommentsResponse }
     * 
     */
    public GetCommentsResponse createGetCommentsResponse() {
        return new GetCommentsResponse();
    }

    /**
     * Create an instance of {@link SignaturePropertyArray }
     * 
     */
    public SignaturePropertyArray createSignaturePropertyArray() {
        return new SignaturePropertyArray();
    }

    /**
     * Create an instance of {@link GetEngineStateRequest }
     * 
     */
    public GetEngineStateRequest createGetEngineStateRequest() {
        return new GetEngineStateRequest();
    }

    /**
     * Create an instance of {@link CommentArray }
     * 
     */
    public CommentArray createCommentArray() {
        return new CommentArray();
    }

    /**
     * Create an instance of {@link ProvisioningRequest }
     * 
     */
    public ProvisioningRequest createProvisioningRequest() {
        return new ProvisioningRequest();
    }

    /**
     * Create an instance of {@link GetProvisioningStatusesRequest }
     * 
     */
    public GetProvisioningStatusesRequest createGetProvisioningStatusesRequest() {
        return new GetProvisioningStatusesRequest();
    }

    /**
     * Create an instance of {@link GetGraphRequest }
     * 
     */
    public GetGraphRequest createGetGraphRequest() {
        return new GetGraphRequest();
    }

    /**
     * Create an instance of {@link ProvisioningStatusArray }
     * 
     */
    public ProvisioningStatusArray createProvisioningStatusArray() {
        return new ProvisioningStatusArray();
    }

    /**
     * Create an instance of {@link GetProcessesByApprovalStatusResponse }
     * 
     */
    public GetProcessesByApprovalStatusResponse createGetProcessesByApprovalStatusResponse() {
        return new GetProcessesByApprovalStatusResponse();
    }

    /**
     * Create an instance of {@link StartAsProxyRequest }
     * 
     */
    public StartAsProxyRequest createStartAsProxyRequest() {
        return new StartAsProxyRequest();
    }

    /**
     * Create an instance of {@link GetProcessesByCreationIntervalResponse }
     * 
     */
    public GetProcessesByCreationIntervalResponse createGetProcessesByCreationIntervalResponse() {
        return new GetProcessesByCreationIntervalResponse();
    }

    /**
     * Create an instance of {@link GetCommentsByCreationTimeRequest }
     * 
     */
    public GetCommentsByCreationTimeRequest createGetCommentsByCreationTimeRequest() {
        return new GetCommentsByCreationTimeRequest();
    }

    /**
     * Create an instance of {@link SetResultRequest }
     * 
     */
    public SetResultRequest createSetResultRequest() {
        return new SetResultRequest();
    }

    /**
     * Create an instance of {@link GetProcessesByCreationTimeRequest }
     * 
     */
    public GetProcessesByCreationTimeRequest createGetProcessesByCreationTimeRequest() {
        return new GetProcessesByCreationTimeRequest();
    }

    /**
     * Create an instance of {@link GetAllProvisioningRequestsResponse }
     * 
     */
    public GetAllProvisioningRequestsResponse createGetAllProvisioningRequestsResponse() {
        return new GetAllProvisioningRequestsResponse();
    }

    /**
     * Create an instance of {@link SetUserActivityTimeoutRequest }
     * 
     */
    public SetUserActivityTimeoutRequest createSetUserActivityTimeoutRequest() {
        return new SetUserActivityTimeoutRequest();
    }

    /**
     * Create an instance of {@link GetUserActivityTimeoutResponse }
     * 
     */
    public GetUserActivityTimeoutResponse createGetUserActivityTimeoutResponse() {
        return new GetUserActivityTimeoutResponse();
    }

    /**
     * Create an instance of {@link GetCommentsByTypeResponse }
     * 
     */
    public GetCommentsByTypeResponse createGetCommentsByTypeResponse() {
        return new GetCommentsByTypeResponse();
    }

    /**
     * Create an instance of {@link TerminateRequest }
     * 
     */
    public TerminateRequest createTerminateRequest() {
        return new TerminateRequest();
    }

    /**
     * Create an instance of {@link StringArray }
     * 
     */
    public StringArray createStringArray() {
        return new StringArray();
    }

    /**
     * Create an instance of {@link SetRoleRequestStatusRequest }
     * 
     */
    public SetRoleRequestStatusRequest createSetRoleRequestStatusRequest() {
        return new SetRoleRequestStatusRequest();
    }

    /**
     * Create an instance of {@link SetRoleRequestStatusResponse }
     * 
     */
    public SetRoleRequestStatusResponse createSetRoleRequestStatusResponse() {
        return new SetRoleRequestStatusResponse();
    }

    /**
     * Create an instance of {@link WorkEntry }
     * 
     */
    public WorkEntry createWorkEntry() {
        return new WorkEntry();
    }

    /**
     * Create an instance of {@link MultiStartResponse }
     * 
     */
    public MultiStartResponse createMultiStartResponse() {
        return new MultiStartResponse();
    }

    /**
     * Create an instance of {@link ReassignWorkTaskResponse }
     * 
     */
    public ReassignWorkTaskResponse createReassignWorkTaskResponse() {
        return new ReassignWorkTaskResponse();
    }

    /**
     * Create an instance of {@link GetEmailNotificationsResponse }
     * 
     */
    public GetEmailNotificationsResponse createGetEmailNotificationsResponse() {
        return new GetEmailNotificationsResponse();
    }

    /**
     * Create an instance of {@link StartAsProxyResponse }
     * 
     */
    public StartAsProxyResponse createStartAsProxyResponse() {
        return new StartAsProxyResponse();
    }

    /**
     * Create an instance of {@link AddCommentRequest }
     * 
     */
    public AddCommentRequest createAddCommentRequest() {
        return new AddCommentRequest();
    }

    /**
     * Create an instance of {@link GetProcessesByStatusRequest }
     * 
     */
    public GetProcessesByStatusRequest createGetProcessesByStatusRequest() {
        return new GetProcessesByStatusRequest();
    }

    /**
     * Create an instance of {@link ForwardAsProxyWithDigitalSignatureResponse }
     * 
     */
    public ForwardAsProxyWithDigitalSignatureResponse createForwardAsProxyWithDigitalSignatureResponse() {
        return new ForwardAsProxyWithDigitalSignatureResponse();
    }

    /**
     * Create an instance of {@link GetClusterStateRequest }
     * 
     */
    public GetClusterStateRequest createGetClusterStateRequest() {
        return new GetClusterStateRequest();
    }

    /**
     * Create an instance of {@link ClearNIMCachesResponse }
     * 
     */
    public ClearNIMCachesResponse createClearNIMCachesResponse() {
        return new ClearNIMCachesResponse();
    }

    /**
     * Create an instance of {@link Comment }
     * 
     */
    public Comment createComment() {
        return new Comment();
    }

    /**
     * Create an instance of {@link SetEngineConfigurationResponse }
     * 
     */
    public SetEngineConfigurationResponse createSetEngineConfigurationResponse() {
        return new SetEngineConfigurationResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "state", scope = TProvisioningStatusQuery.class)
    public JAXBElement<Integer> createTProvisioningStatusQueryState(Integer value) {
        return new JAXBElement<Integer>(_TProvisioningStatusQueryState_QNAME, Integer.class, TProvisioningStatusQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TTime }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "resultTime", scope = TProvisioningStatusQuery.class)
    public JAXBElement<TTime> createTProvisioningStatusQueryResultTime(TTime value) {
        return new JAXBElement<TTime>(_TProvisioningStatusQueryResultTime_QNAME, TTime.class, TProvisioningStatusQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "recipient", scope = TProvisioningStatusQuery.class)
    public JAXBElement<String> createTProvisioningStatusQueryRecipient(String value) {
        return new JAXBElement<String>(_TProvisioningStatusQueryRecipient_QNAME, String.class, TProvisioningStatusQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TTime }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "provisioningTime", scope = TProvisioningStatusQuery.class)
    public JAXBElement<TTime> createTProvisioningStatusQueryProvisioningTime(TTime value) {
        return new JAXBElement<TTime>(_TProvisioningStatusQueryProvisioningTime_QNAME, TTime.class, TProvisioningStatusQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "activityId", scope = TProvisioningStatusQuery.class)
    public JAXBElement<String> createTProvisioningStatusQueryActivityId(String value) {
        return new JAXBElement<String>(_TProvisioningStatusQueryActivityId_QNAME, String.class, TProvisioningStatusQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "requestId", scope = TProvisioningStatusQuery.class)
    public JAXBElement<String> createTProvisioningStatusQueryRequestId(String value) {
        return new JAXBElement<String>(_TProvisioningStatusQueryRequestId_QNAME, String.class, TProvisioningStatusQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "status", scope = TProvisioningStatusQuery.class)
    public JAXBElement<Integer> createTProvisioningStatusQueryStatus(Integer value) {
        return new JAXBElement<Integer>(_TProvisioningStatusQueryStatus_QNAME, Integer.class, TProvisioningStatusQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllProvisioningRequestsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getAllProvisioningRequestsRequest")
    public JAXBElement<GetAllProvisioningRequestsRequest> createGetAllProvisioningRequestsRequest(GetAllProvisioningRequestsRequest value) {
        return new JAXBElement<GetAllProvisioningRequestsRequest>(_GetAllProvisioningRequestsRequest_QNAME, GetAllProvisioningRequestsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveEngineRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "removeEngineRequest")
    public JAXBElement<RemoveEngineRequest> createRemoveEngineRequest(RemoveEngineRequest value) {
        return new JAXBElement<RemoveEngineRequest>(_RemoveEngineRequest_QNAME, RemoveEngineRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TProvisioningStatusQuery }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "ProvisioningStatusQuery")
    public JAXBElement<TProvisioningStatusQuery> createProvisioningStatusQuery(TProvisioningStatusQuery value) {
        return new JAXBElement<TProvisioningStatusQuery>(_ProvisioningStatusQuery_QNAME, TProvisioningStatusQuery.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClusterStateRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getClusterStateRequest")
    public JAXBElement<GetClusterStateRequest> createGetClusterStateRequest(GetClusterStateRequest value) {
        return new JAXBElement<GetClusterStateRequest>(_GetClusterStateRequest_QNAME, GetClusterStateRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllProcessesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getAllProcessesResponse")
    public JAXBElement<GetAllProcessesResponse> createGetAllProcessesResponse(GetAllProcessesResponse value) {
        return new JAXBElement<GetAllProcessesResponse>(_GetAllProcessesResponse_QNAME, GetAllProcessesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearNIMCachesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "clearNIMCachesRequest")
    public JAXBElement<ClearNIMCachesRequest> createClearNIMCachesRequest(ClearNIMCachesRequest value) {
        return new JAXBElement<ClearNIMCachesRequest>(_ClearNIMCachesRequest_QNAME, ClearNIMCachesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesArrayResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesArrayResponse")
    public JAXBElement<GetProcessesArrayResponse> createGetProcessesArrayResponse(GetProcessesArrayResponse value) {
        return new JAXBElement<GetProcessesArrayResponse>(_GetProcessesArrayResponse_QNAME, GetProcessesArrayResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProvisioningRequestsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProvisioningRequestsRequest")
    public JAXBElement<GetProvisioningRequestsRequest> createGetProvisioningRequestsRequest(GetProvisioningRequestsRequest value) {
        return new JAXBElement<GetProvisioningRequestsRequest>(_GetProvisioningRequestsRequest_QNAME, GetProvisioningRequestsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartAsProxyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "startAsProxyResponse")
    public JAXBElement<StartAsProxyResponse> createStartAsProxyResponse(StartAsProxyResponse value) {
        return new JAXBElement<StartAsProxyResponse>(_StartAsProxyResponse_QNAME, StartAsProxyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TAction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "Action")
    public JAXBElement<TAction> createAction(TAction value) {
        return new JAXBElement<TAction>(_Action_QNAME, TAction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessRequest")
    public JAXBElement<GetProcessRequest> createGetProcessRequest(GetProcessRequest value) {
        return new JAXBElement<GetProcessRequest>(_GetProcessRequest_QNAME, GetProcessRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "startResponse")
    public JAXBElement<StartResponse> createStartResponse(StartResponse value) {
        return new JAXBElement<StartResponse>(_StartResponse_QNAME, StartResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminateRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "terminateRequest")
    public JAXBElement<TerminateRequest> createTerminateRequest(TerminateRequest value) {
        return new JAXBElement<TerminateRequest>(_TerminateRequest_QNAME, TerminateRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AvailableActionArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "AvailableActionArray")
    public JAXBElement<AvailableActionArray> createAvailableActionArray(AvailableActionArray value) {
        return new JAXBElement<AvailableActionArray>(_AvailableActionArray_QNAME, AvailableActionArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCommentsByUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getCommentsByUserResponse")
    public JAXBElement<GetCommentsByUserResponse> createGetCommentsByUserResponse(GetCommentsByUserResponse value) {
        return new JAXBElement<GetCommentsByUserResponse>(_GetCommentsByUserResponse_QNAME, GetCommentsByUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReassignWorkTaskResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "reassignWorkTaskResponse")
    public JAXBElement<ReassignWorkTaskResponse> createReassignWorkTaskResponse(ReassignWorkTaskResponse value) {
        return new JAXBElement<ReassignWorkTaskResponse>(_ReassignWorkTaskResponse_QNAME, ReassignWorkTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEmailNotificationsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getEmailNotificationsRequest")
    public JAXBElement<GetEmailNotificationsRequest> createGetEmailNotificationsRequest(GetEmailNotificationsRequest value) {
        return new JAXBElement<GetEmailNotificationsRequest>(_GetEmailNotificationsRequest_QNAME, GetEmailNotificationsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearNIMCachesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "clearNIMCachesResponse")
    public JAXBElement<ClearNIMCachesResponse> createClearNIMCachesResponse(ClearNIMCachesResponse value) {
        return new JAXBElement<ClearNIMCachesResponse>(_ClearNIMCachesResponse_QNAME, ClearNIMCachesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetWebServiceActivityTimeoutRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "setWebServiceActivityTimeoutRequest")
    public JAXBElement<SetWebServiceActivityTimeoutRequest> createSetWebServiceActivityTimeoutRequest(SetWebServiceActivityTimeoutRequest value) {
        return new JAXBElement<SetWebServiceActivityTimeoutRequest>(_SetWebServiceActivityTimeoutRequest_QNAME, SetWebServiceActivityTimeoutRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCommentsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getCommentsRequest")
    public JAXBElement<GetCommentsRequest> createGetCommentsRequest(GetCommentsRequest value) {
        return new JAXBElement<GetCommentsRequest>(_GetCommentsRequest_QNAME, GetCommentsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetQuorumForWorkTaskRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getQuorumForWorkTaskRequest")
    public JAXBElement<GetQuorumForWorkTaskRequest> createGetQuorumForWorkTaskRequest(GetQuorumForWorkTaskRequest value) {
        return new JAXBElement<GetQuorumForWorkTaskRequest>(_GetQuorumForWorkTaskRequest_QNAME, GetQuorumForWorkTaskRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReassignProcessesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "reassignProcessesResponse")
    public JAXBElement<ReassignProcessesResponse> createReassignProcessesResponse(ReassignProcessesResponse value) {
        return new JAXBElement<ReassignProcessesResponse>(_ReassignProcessesResponse_QNAME, ReassignProcessesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEmailNotificationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getEmailNotificationsResponse")
    public JAXBElement<GetEmailNotificationsResponse> createGetEmailNotificationsResponse(GetEmailNotificationsResponse value) {
        return new JAXBElement<GetEmailNotificationsResponse>(_GetEmailNotificationsResponse_QNAME, GetEmailNotificationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "startRequest")
    public JAXBElement<StartRequest> createStartRequest(StartRequest value) {
        return new JAXBElement<StartRequest>(_StartRequest_QNAME, StartRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReassignWorkTaskRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "reassignWorkTaskRequest")
    public JAXBElement<ReassignWorkTaskRequest> createReassignWorkTaskRequest(ReassignWorkTaskRequest value) {
        return new JAXBElement<ReassignWorkTaskRequest>(_ReassignWorkTaskRequest_QNAME, ReassignWorkTaskRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TEngineStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "EngineStatus")
    public JAXBElement<TEngineStatus> createEngineStatus(TEngineStatus value) {
        return new JAXBElement<TEngineStatus>(_EngineStatus_QNAME, TEngineStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetEngineConfigurationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "setEngineConfigurationResponse")
    public JAXBElement<SetEngineConfigurationResponse> createSetEngineConfigurationResponse(SetEngineConfigurationResponse value) {
        return new JAXBElement<SetEngineConfigurationResponse>(_SetEngineConfigurationResponse_QNAME, SetEngineConfigurationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCompletedProcessTimeoutRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getCompletedProcessTimeoutRequest")
    public JAXBElement<GetCompletedProcessTimeoutRequest> createGetCompletedProcessTimeoutRequest(GetCompletedProcessTimeoutRequest value) {
        return new JAXBElement<GetCompletedProcessTimeoutRequest>(_GetCompletedProcessTimeoutRequest_QNAME, GetCompletedProcessTimeoutRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEngineConfigurationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getEngineConfigurationResponse")
    public JAXBElement<GetEngineConfigurationResponse> createGetEngineConfigurationResponse(GetEngineConfigurationResponse value) {
        return new JAXBElement<GetEngineConfigurationResponse>(_GetEngineConfigurationResponse_QNAME, GetEngineConfigurationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetUserActivityTimeoutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "setUserActivityTimeoutResponse")
    public JAXBElement<SetUserActivityTimeoutResponse> createSetUserActivityTimeoutResponse(SetUserActivityTimeoutResponse value) {
        return new JAXBElement<SetUserActivityTimeoutResponse>(_SetUserActivityTimeoutResponse_QNAME, SetUserActivityTimeoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProvisioningStatusesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProvisioningStatusesResponse")
    public JAXBElement<GetProvisioningStatusesResponse> createGetProvisioningStatusesResponse(GetProvisioningStatusesResponse value) {
        return new JAXBElement<GetProvisioningStatusesResponse>(_GetProvisioningStatusesResponse_QNAME, GetProvisioningStatusesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataItemArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "DataItemArray")
    public JAXBElement<DataItemArray> createDataItemArray(DataItemArray value) {
        return new JAXBElement<DataItemArray>(_DataItemArray_QNAME, DataItemArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResetPriorityForWorkTaskRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "resetPriorityForWorkTaskRequest")
    public JAXBElement<ResetPriorityForWorkTaskRequest> createResetPriorityForWorkTaskRequest(ResetPriorityForWorkTaskRequest value) {
        return new JAXBElement<ResetPriorityForWorkTaskRequest>(_ResetPriorityForWorkTaskRequest_QNAME, ResetPriorityForWorkTaskRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "terminateResponse")
    public JAXBElement<TerminateResponse> createTerminateResponse(TerminateResponse value) {
        return new JAXBElement<TerminateResponse>(_TerminateResponse_QNAME, TerminateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiveRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "receiveRequest")
    public JAXBElement<ReceiveRequest> createReceiveRequest(ReceiveRequest value) {
        return new JAXBElement<ReceiveRequest>(_ReceiveRequest_QNAME, ReceiveRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReassignResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "reassignResponse")
    public JAXBElement<ReassignResponse> createReassignResponse(ReassignResponse value) {
        return new JAXBElement<ReassignResponse>(_ReassignResponse_QNAME, ReassignResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetEmailNotificationsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "setEmailNotificationsRequest")
    public JAXBElement<SetEmailNotificationsRequest> createSetEmailNotificationsRequest(SetEmailNotificationsRequest value) {
        return new JAXBElement<SetEmailNotificationsRequest>(_SetEmailNotificationsRequest_QNAME, SetEmailNotificationsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVersionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getVersionRequest")
    public JAXBElement<GetVersionRequest> createGetVersionRequest(GetVersionRequest value) {
        return new JAXBElement<GetVersionRequest>(_GetVersionRequest_QNAME, GetVersionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TCommentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "CommentType")
    public JAXBElement<TCommentType> createCommentType(TCommentType value) {
        return new JAXBElement<TCommentType>(_CommentType_QNAME, TCommentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClusterStateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getClusterStateResponse")
    public JAXBElement<GetClusterStateResponse> createGetClusterStateResponse(GetClusterStateResponse value) {
        return new JAXBElement<GetClusterStateResponse>(_GetClusterStateResponse_QNAME, GetClusterStateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReassignRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "reassignRequest")
    public JAXBElement<ReassignRequest> createReassignRequest(ReassignRequest value) {
        return new JAXBElement<ReassignRequest>(_ReassignRequest_QNAME, ReassignRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByRecipientRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByRecipientRequest")
    public JAXBElement<GetProcessesByRecipientRequest> createGetProcessesByRecipientRequest(GetProcessesByRecipientRequest value) {
        return new JAXBElement<GetProcessesByRecipientRequest>(_GetProcessesByRecipientRequest_QNAME, GetProcessesByRecipientRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ForwardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "forwardResponse")
    public JAXBElement<ForwardResponse> createForwardResponse(ForwardResponse value) {
        return new JAXBElement<ForwardResponse>(_ForwardResponse_QNAME, ForwardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByStatusRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByStatusRequest")
    public JAXBElement<GetProcessesByStatusRequest> createGetProcessesByStatusRequest(GetProcessesByStatusRequest value) {
        return new JAXBElement<GetProcessesByStatusRequest>(_GetProcessesByStatusRequest_QNAME, GetProcessesByStatusRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TOperator }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "Operator")
    public JAXBElement<TOperator> createOperator(TOperator value) {
        return new JAXBElement<TOperator>(_Operator_QNAME, TOperator.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TEntitlementState }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "EntitlementState")
    public JAXBElement<TEntitlementState> createEntitlementState(TEntitlementState value) {
        return new JAXBElement<TEntitlementState>(_EntitlementState_QNAME, TEntitlementState.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCommentsByActivityResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getCommentsByActivityResponse")
    public JAXBElement<GetCommentsByActivityResponse> createGetCommentsByActivityResponse(GetCommentsByActivityResponse value) {
        return new JAXBElement<GetCommentsByActivityResponse>(_GetCommentsByActivityResponse_QNAME, GetCommentsByActivityResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResetPriorityForWorkTaskResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "resetPriorityForWorkTaskResponse")
    public JAXBElement<ResetPriorityForWorkTaskResponse> createResetPriorityForWorkTaskResponse(ResetPriorityForWorkTaskResponse value) {
        return new JAXBElement<ResetPriorityForWorkTaskResponse>(_ResetPriorityForWorkTaskResponse_QNAME, ResetPriorityForWorkTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByCreationTimeRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByCreationTimeRequest")
    public JAXBElement<GetProcessesByCreationTimeRequest> createGetProcessesByCreationTimeRequest(GetProcessesByCreationTimeRequest value) {
        return new JAXBElement<GetProcessesByCreationTimeRequest>(_GetProcessesByCreationTimeRequest_QNAME, GetProcessesByCreationTimeRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFormDefinitionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getFormDefinitionRequest")
    public JAXBElement<GetFormDefinitionRequest> createGetFormDefinitionRequest(GetFormDefinitionRequest value) {
        return new JAXBElement<GetFormDefinitionRequest>(_GetFormDefinitionRequest_QNAME, GetFormDefinitionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ForwardAsProxyWithDigitalSignatureRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "forwardAsProxyWithDigitalSignatureRequest")
    public JAXBElement<ForwardAsProxyWithDigitalSignatureRequest> createForwardAsProxyWithDigitalSignatureRequest(ForwardAsProxyWithDigitalSignatureRequest value) {
        return new JAXBElement<ForwardAsProxyWithDigitalSignatureRequest>(_ForwardAsProxyWithDigitalSignatureRequest_QNAME, ForwardAsProxyWithDigitalSignatureRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByIdResponse")
    public JAXBElement<GetProcessesByIdResponse> createGetProcessesByIdResponse(GetProcessesByIdResponse value) {
        return new JAXBElement<GetProcessesByIdResponse>(_GetProcessesByIdResponse_QNAME, GetProcessesByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartWithCorrelationIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "startWithCorrelationIdResponse")
    public JAXBElement<StartWithCorrelationIdResponse> createStartWithCorrelationIdResponse(StartWithCorrelationIdResponse value) {
        return new JAXBElement<StartWithCorrelationIdResponse>(_StartWithCorrelationIdResponse_QNAME, StartWithCorrelationIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByCreationTimeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByCreationTimeResponse")
    public JAXBElement<GetProcessesByCreationTimeResponse> createGetProcessesByCreationTimeResponse(GetProcessesByCreationTimeResponse value) {
        return new JAXBElement<GetProcessesByCreationTimeResponse>(_GetProcessesByCreationTimeResponse_QNAME, GetProcessesByCreationTimeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByQueryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByQueryResponse")
    public JAXBElement<GetProcessesByQueryResponse> createGetProcessesByQueryResponse(GetProcessesByQueryResponse value) {
        return new JAXBElement<GetProcessesByQueryResponse>(_GetProcessesByQueryResponse_QNAME, GetProcessesByQueryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ForwardWithDigitalSignatureRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "forwardWithDigitalSignatureRequest")
    public JAXBElement<ForwardWithDigitalSignatureRequest> createForwardWithDigitalSignatureRequest(ForwardWithDigitalSignatureRequest value) {
        return new JAXBElement<ForwardWithDigitalSignatureRequest>(_ForwardWithDigitalSignatureRequest_QNAME, ForwardWithDigitalSignatureRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesResponse")
    public JAXBElement<GetProcessesResponse> createGetProcessesResponse(GetProcessesResponse value) {
        return new JAXBElement<GetProcessesResponse>(_GetProcessesResponse_QNAME, GetProcessesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCompletedProcessTimeoutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getCompletedProcessTimeoutResponse")
    public JAXBElement<GetCompletedProcessTimeoutResponse> createGetCompletedProcessTimeoutResponse(GetCompletedProcessTimeoutResponse value) {
        return new JAXBElement<GetCompletedProcessTimeoutResponse>(_GetCompletedProcessTimeoutResponse_QNAME, GetCompletedProcessTimeoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AvailableAction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "AvailableAction")
    public JAXBElement<AvailableAction> createAvailableAction(AvailableAction value) {
        return new JAXBElement<AvailableAction>(_AvailableAction_QNAME, AvailableAction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWebServiceActivityTimeoutRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getWebServiceActivityTimeoutRequest")
    public JAXBElement<GetWebServiceActivityTimeoutRequest> createGetWebServiceActivityTimeoutRequest(GetWebServiceActivityTimeoutRequest value) {
        return new JAXBElement<GetWebServiceActivityTimeoutRequest>(_GetWebServiceActivityTimeoutRequest_QNAME, GetWebServiceActivityTimeoutRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Comment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "Comment")
    public JAXBElement<Comment> createComment(Comment value) {
        return new JAXBElement<Comment>(_Comment_QNAME, Comment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartWithDigitalSignatureResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "startWithDigitalSignatureResponse")
    public JAXBElement<StartWithDigitalSignatureResponse> createStartWithDigitalSignatureResponse(StartWithDigitalSignatureResponse value) {
        return new JAXBElement<StartWithDigitalSignatureResponse>(_StartWithDigitalSignatureResponse_QNAME, StartWithDigitalSignatureResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEngineStateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getEngineStateResponse")
    public JAXBElement<GetEngineStateResponse> createGetEngineStateResponse(GetEngineStateResponse value) {
        return new JAXBElement<GetEngineStateResponse>(_GetEngineStateResponse_QNAME, GetEngineStateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "DataItem")
    public JAXBElement<DataItem> createDataItem(DataItem value) {
        return new JAXBElement<DataItem>(_DataItem_QNAME, DataItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProvisioningRequestsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProvisioningRequestsResponse")
    public JAXBElement<GetProvisioningRequestsResponse> createGetProvisioningRequestsResponse(GetProvisioningRequestsResponse value) {
        return new JAXBElement<GetProvisioningRequestsResponse>(_GetProvisioningRequestsResponse_QNAME, GetProvisioningRequestsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetWebServiceActivityTimeoutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "setWebServiceActivityTimeoutResponse")
    public JAXBElement<SetWebServiceActivityTimeoutResponse> createSetWebServiceActivityTimeoutResponse(SetWebServiceActivityTimeoutResponse value) {
        return new JAXBElement<SetWebServiceActivityTimeoutResponse>(_SetWebServiceActivityTimeoutResponse_QNAME, SetWebServiceActivityTimeoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCommentsByTypeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getCommentsByTypeResponse")
    public JAXBElement<GetCommentsByTypeResponse> createGetCommentsByTypeResponse(GetCommentsByTypeResponse value) {
        return new JAXBElement<GetCommentsByTypeResponse>(_GetCommentsByTypeResponse_QNAME, GetCommentsByTypeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ForwardWithDigitalSignatureResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "forwardWithDigitalSignatureResponse")
    public JAXBElement<ForwardWithDigitalSignatureResponse> createForwardWithDigitalSignatureResponse(ForwardWithDigitalSignatureResponse value) {
        return new JAXBElement<ForwardWithDigitalSignatureResponse>(_ForwardWithDigitalSignatureResponse_QNAME, ForwardWithDigitalSignatureResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartAsProxyWithDigitalSignatureResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "startAsProxyWithDigitalSignatureResponse")
    public JAXBElement<StartAsProxyWithDigitalSignatureResponse> createStartAsProxyWithDigitalSignatureResponse(StartAsProxyWithDigitalSignatureResponse value) {
        return new JAXBElement<StartAsProxyWithDigitalSignatureResponse>(_StartAsProxyWithDigitalSignatureResponse_QNAME, StartAsProxyWithDigitalSignatureResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "ProcessArray")
    public JAXBElement<ProcessArray> createProcessArray(ProcessArray value) {
        return new JAXBElement<ProcessArray>(_ProcessArray_QNAME, ProcessArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnclaimResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "unclaimResponse")
    public JAXBElement<UnclaimResponse> createUnclaimResponse(UnclaimResponse value) {
        return new JAXBElement<UnclaimResponse>(_UnclaimResponse_QNAME, UnclaimResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetEmailNotificationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "setEmailNotificationsResponse")
    public JAXBElement<SetEmailNotificationsResponse> createSetEmailNotificationsResponse(SetEmailNotificationsResponse value) {
        return new JAXBElement<SetEmailNotificationsResponse>(_SetEmailNotificationsResponse_QNAME, SetEmailNotificationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ForwardAsProxyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "forwardAsProxyResponse")
    public JAXBElement<ForwardAsProxyResponse> createForwardAsProxyResponse(ForwardAsProxyResponse value) {
        return new JAXBElement<ForwardAsProxyResponse>(_ForwardAsProxyResponse_QNAME, ForwardAsProxyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommentArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "CommentArray")
    public JAXBElement<CommentArray> createCommentArray(CommentArray value) {
        return new JAXBElement<CommentArray>(_CommentArray_QNAME, CommentArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserActivityTimeoutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getUserActivityTimeoutResponse")
    public JAXBElement<GetUserActivityTimeoutResponse> createGetUserActivityTimeoutResponse(GetUserActivityTimeoutResponse value) {
        return new JAXBElement<GetUserActivityTimeoutResponse>(_GetUserActivityTimeoutResponse_QNAME, GetUserActivityTimeoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "receiveResponse")
    public JAXBElement<ReceiveResponse> createReceiveResponse(ReceiveResponse value) {
        return new JAXBElement<ReceiveResponse>(_ReceiveResponse_QNAME, ReceiveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MultiStartRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "multiStartRequest")
    public JAXBElement<MultiStartRequest> createMultiStartRequest(MultiStartRequest value) {
        return new JAXBElement<MultiStartRequest>(_MultiStartRequest_QNAME, MultiStartRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvisioningStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "ProvisioningStatus")
    public JAXBElement<ProvisioningStatus> createProvisioningStatus(ProvisioningStatus value) {
        return new JAXBElement<ProvisioningStatus>(_ProvisioningStatus_QNAME, ProvisioningStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByStatusResponse")
    public JAXBElement<GetProcessesByStatusResponse> createGetProcessesByStatusResponse(GetProcessesByStatusResponse value) {
        return new JAXBElement<GetProcessesByStatusResponse>(_GetProcessesByStatusResponse_QNAME, GetProcessesByStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StringArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "StringArray")
    public JAXBElement<StringArray> createStringArray(StringArray value) {
        return new JAXBElement<StringArray>(_StringArray_QNAME, StringArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByRecipientResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByRecipientResponse")
    public JAXBElement<GetProcessesByRecipientResponse> createGetProcessesByRecipientResponse(GetProcessesByRecipientResponse value) {
        return new JAXBElement<GetProcessesByRecipientResponse>(_GetProcessesByRecipientResponse_QNAME, GetProcessesByRecipientResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetResultResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "setResultResponse")
    public JAXBElement<SetResultResponse> createSetResultResponse(SetResultResponse value) {
        return new JAXBElement<SetResultResponse>(_SetResultResponse_QNAME, SetResultResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReassignProcessesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "reassignProcessesRequest")
    public JAXBElement<ReassignProcessesRequest> createReassignProcessesRequest(ReassignProcessesRequest value) {
        return new JAXBElement<ReassignProcessesRequest>(_ReassignProcessesRequest_QNAME, ReassignProcessesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEngineConfigurationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getEngineConfigurationRequest")
    public JAXBElement<GetEngineConfigurationRequest> createGetEngineConfigurationRequest(GetEngineConfigurationRequest value) {
        return new JAXBElement<GetEngineConfigurationRequest>(_GetEngineConfigurationRequest_QNAME, GetEngineConfigurationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignaturePropertyArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "SignaturePropertyArray")
    public JAXBElement<SignaturePropertyArray> createSignaturePropertyArray(SignaturePropertyArray value) {
        return new JAXBElement<SignaturePropertyArray>(_SignaturePropertyArray_QNAME, SignaturePropertyArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MultiStartResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "multiStartResponse")
    public JAXBElement<MultiStartResponse> createMultiStartResponse(MultiStartResponse value) {
        return new JAXBElement<MultiStartResponse>(_MultiStartResponse_QNAME, MultiStartResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdminException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "AdminException")
    public JAXBElement<AdminException> createAdminException(AdminException value) {
        return new JAXBElement<AdminException>(_AdminException_QNAME, AdminException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCommentsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getCommentsResponse")
    public JAXBElement<GetCommentsResponse> createGetCommentsResponse(GetCommentsResponse value) {
        return new JAXBElement<GetCommentsResponse>(_GetCommentsResponse_QNAME, GetCommentsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ForwardRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "forwardRequest")
    public JAXBElement<ForwardRequest> createForwardRequest(ForwardRequest value) {
        return new JAXBElement<ForwardRequest>(_ForwardRequest_QNAME, ForwardRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReassignAllProcessesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "reassignAllProcessesResponse")
    public JAXBElement<ReassignAllProcessesResponse> createReassignAllProcessesResponse(ReassignAllProcessesResponse value) {
        return new JAXBElement<ReassignAllProcessesResponse>(_ReassignAllProcessesResponse_QNAME, ReassignAllProcessesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetResultRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "setResultRequest")
    public JAXBElement<SetResultRequest> createSetResultRequest(SetResultRequest value) {
        return new JAXBElement<SetResultRequest>(_SetResultRequest_QNAME, SetResultRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCommentsByCreationTimeRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getCommentsByCreationTimeRequest")
    public JAXBElement<GetCommentsByCreationTimeRequest> createGetCommentsByCreationTimeRequest(GetCommentsByCreationTimeRequest value) {
        return new JAXBElement<GetCommentsByCreationTimeRequest>(_GetCommentsByCreationTimeRequest_QNAME, GetCommentsByCreationTimeRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCommentRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "addCommentRequest")
    public JAXBElement<AddCommentRequest> createAddCommentRequest(AddCommentRequest value) {
        return new JAXBElement<AddCommentRequest>(_AddCommentRequest_QNAME, AddCommentRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TApprovalStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "ApprovalStatus")
    public JAXBElement<TApprovalStatus> createApprovalStatus(TApprovalStatus value) {
        return new JAXBElement<TApprovalStatus>(_ApprovalStatus_QNAME, TApprovalStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByInitiatorRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByInitiatorRequest")
    public JAXBElement<GetProcessesByInitiatorRequest> createGetProcessesByInitiatorRequest(GetProcessesByInitiatorRequest value) {
        return new JAXBElement<GetProcessesByInitiatorRequest>(_GetProcessesByInitiatorRequest_QNAME, GetProcessesByInitiatorRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvisioningRequestArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "ProvisioningRequestArray")
    public JAXBElement<ProvisioningRequestArray> createProvisioningRequestArray(ProvisioningRequestArray value) {
        return new JAXBElement<ProvisioningRequestArray>(_ProvisioningRequestArray_QNAME, ProvisioningRequestArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TTerminationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "TerminationType")
    public JAXBElement<TTerminationType> createTerminationType(TTerminationType value) {
        return new JAXBElement<TTerminationType>(_TerminationType_QNAME, TTerminationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWorkResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getWorkResponse")
    public JAXBElement<GetWorkResponse> createGetWorkResponse(GetWorkResponse value) {
        return new JAXBElement<GetWorkResponse>(_GetWorkResponse_QNAME, GetWorkResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCommentsByCreationTimeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getCommentsByCreationTimeResponse")
    public JAXBElement<GetCommentsByCreationTimeResponse> createGetCommentsByCreationTimeResponse(GetCommentsByCreationTimeResponse value) {
        return new JAXBElement<GetCommentsByCreationTimeResponse>(_GetCommentsByCreationTimeResponse_QNAME, GetCommentsByCreationTimeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TEntitlementStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "EntitlementStatus")
    public JAXBElement<TEntitlementStatus> createEntitlementStatus(TEntitlementStatus value) {
        return new JAXBElement<TEntitlementStatus>(_EntitlementStatus_QNAME, TEntitlementStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReassignPercentageProcessesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "reassignPercentageProcessesRequest")
    public JAXBElement<ReassignPercentageProcessesRequest> createReassignPercentageProcessesRequest(ReassignPercentageProcessesRequest value) {
        return new JAXBElement<ReassignPercentageProcessesRequest>(_ReassignPercentageProcessesRequest_QNAME, ReassignPercentageProcessesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Quorum }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "Quorum")
    public JAXBElement<Quorum> createQuorum(Quorum value) {
        return new JAXBElement<Quorum>(_Quorum_QNAME, Quorum.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetUserActivityTimeoutRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "setUserActivityTimeoutRequest")
    public JAXBElement<SetUserActivityTimeoutRequest> createSetUserActivityTimeoutRequest(SetUserActivityTimeoutRequest value) {
        return new JAXBElement<SetUserActivityTimeoutRequest>(_SetUserActivityTimeoutRequest_QNAME, SetUserActivityTimeoutRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByCreationIntervalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByCreationIntervalResponse")
    public JAXBElement<GetProcessesByCreationIntervalResponse> createGetProcessesByCreationIntervalResponse(GetProcessesByCreationIntervalResponse value) {
        return new JAXBElement<GetProcessesByCreationIntervalResponse>(_GetProcessesByCreationIntervalResponse_QNAME, GetProcessesByCreationIntervalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TProcessInfoQuery }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "ProcessInfoQuery")
    public JAXBElement<TProcessInfoQuery> createProcessInfoQuery(TProcessInfoQuery value) {
        return new JAXBElement<TProcessInfoQuery>(_ProcessInfoQuery_QNAME, TProcessInfoQuery.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveEngineResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "removeEngineResponse")
    public JAXBElement<RemoveEngineResponse> createRemoveEngineResponse(RemoveEngineResponse value) {
        return new JAXBElement<RemoveEngineResponse>(_RemoveEngineResponse_QNAME, RemoveEngineResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesRequest")
    public JAXBElement<GetProcessesRequest> createGetProcessesRequest(GetProcessesRequest value) {
        return new JAXBElement<GetProcessesRequest>(_GetProcessesRequest_QNAME, GetProcessesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Configuration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "Configuration")
    public JAXBElement<Configuration> createConfiguration(Configuration value) {
        return new JAXBElement<Configuration>(_Configuration_QNAME, Configuration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetEngineConfigurationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "setEngineConfigurationRequest")
    public JAXBElement<SetEngineConfigurationRequest> createSetEngineConfigurationRequest(SetEngineConfigurationRequest value) {
        return new JAXBElement<SetEngineConfigurationRequest>(_SetEngineConfigurationRequest_QNAME, SetEngineConfigurationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByIdRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByIdRequest")
    public JAXBElement<GetProcessesByIdRequest> createGetProcessesByIdRequest(GetProcessesByIdRequest value) {
        return new JAXBElement<GetProcessesByIdRequest>(_GetProcessesByIdRequest_QNAME, GetProcessesByIdRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesArrayRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesArrayRequest")
    public JAXBElement<GetProcessesArrayRequest> createGetProcessesArrayRequest(GetProcessesArrayRequest value) {
        return new JAXBElement<GetProcessesArrayRequest>(_GetProcessesArrayRequest_QNAME, GetProcessesArrayRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFlowDefinitionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getFlowDefinitionResponse")
    public JAXBElement<GetFlowDefinitionResponse> createGetFlowDefinitionResponse(GetFlowDefinitionResponse value) {
        return new JAXBElement<GetFlowDefinitionResponse>(_GetFlowDefinitionResponse_QNAME, GetFlowDefinitionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCommentsByTypeRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getCommentsByTypeRequest")
    public JAXBElement<GetCommentsByTypeRequest> createGetCommentsByTypeRequest(GetCommentsByTypeRequest value) {
        return new JAXBElement<GetCommentsByTypeRequest>(_GetCommentsByTypeRequest_QNAME, GetCommentsByTypeRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartAsProxyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "startAsProxyRequest")
    public JAXBElement<StartAsProxyRequest> createStartAsProxyRequest(StartAsProxyRequest value) {
        return new JAXBElement<StartAsProxyRequest>(_StartAsProxyRequest_QNAME, StartAsProxyRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEngineStateRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getEngineStateRequest")
    public JAXBElement<GetEngineStateRequest> createGetEngineStateRequest(GetEngineStateRequest value) {
        return new JAXBElement<GetEngineStateRequest>(_GetEngineStateRequest_QNAME, GetEngineStateRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EngineState }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "EngineState")
    public JAXBElement<EngineState> createEngineState(EngineState value) {
        return new JAXBElement<EngineState>(_EngineState_QNAME, EngineState.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByCreationIntervalRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByCreationIntervalRequest")
    public JAXBElement<GetProcessesByCreationIntervalRequest> createGetProcessesByCreationIntervalRequest(GetProcessesByCreationIntervalRequest value) {
        return new JAXBElement<GetProcessesByCreationIntervalRequest>(_GetProcessesByCreationIntervalRequest_QNAME, GetProcessesByCreationIntervalRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGraphResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getGraphResponse")
    public JAXBElement<GetGraphResponse> createGetGraphResponse(GetGraphResponse value) {
        return new JAXBElement<GetGraphResponse>(_GetGraphResponse_QNAME, GetGraphResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserActivityTimeoutRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getUserActivityTimeoutRequest")
    public JAXBElement<GetUserActivityTimeoutRequest> createGetUserActivityTimeoutRequest(GetUserActivityTimeoutRequest value) {
        return new JAXBElement<GetUserActivityTimeoutRequest>(_GetUserActivityTimeoutRequest_QNAME, GetUserActivityTimeoutRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TProcessStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "ProcessStatus")
    public JAXBElement<TProcessStatus> createProcessStatus(TProcessStatus value) {
        return new JAXBElement<TProcessStatus>(_ProcessStatus_QNAME, TProcessStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReassignPercentageProcessesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "reassignPercentageProcessesResponse")
    public JAXBElement<ReassignPercentageProcessesResponse> createReassignPercentageProcessesResponse(ReassignPercentageProcessesResponse value) {
        return new JAXBElement<ReassignPercentageProcessesResponse>(_ReassignPercentageProcessesResponse_QNAME, ReassignPercentageProcessesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFlowDefinitionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getFlowDefinitionRequest")
    public JAXBElement<GetFlowDefinitionRequest> createGetFlowDefinitionRequest(GetFlowDefinitionRequest value) {
        return new JAXBElement<GetFlowDefinitionRequest>(_GetFlowDefinitionRequest_QNAME, GetFlowDefinitionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TVersion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "Version")
    public JAXBElement<TVersion> createVersion(TVersion value) {
        return new JAXBElement<TVersion>(_Version_QNAME, TVersion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCommentsByActivityRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getCommentsByActivityRequest")
    public JAXBElement<GetCommentsByActivityRequest> createGetCommentsByActivityRequest(GetCommentsByActivityRequest value) {
        return new JAXBElement<GetCommentsByActivityRequest>(_GetCommentsByActivityRequest_QNAME, GetCommentsByActivityRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EngineStateArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "EngineStateArray")
    public JAXBElement<EngineStateArray> createEngineStateArray(EngineStateArray value) {
        return new JAXBElement<EngineStateArray>(_EngineStateArray_QNAME, EngineStateArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Process }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "Process")
    public JAXBElement<Process> createProcess(Process value) {
        return new JAXBElement<Process>(_Process_QNAME, Process.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByApprovalStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByApprovalStatusResponse")
    public JAXBElement<GetProcessesByApprovalStatusResponse> createGetProcessesByApprovalStatusResponse(GetProcessesByApprovalStatusResponse value) {
        return new JAXBElement<GetProcessesByApprovalStatusResponse>(_GetProcessesByApprovalStatusResponse_QNAME, GetProcessesByApprovalStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignatureProperty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "SignatureProperty")
    public JAXBElement<SignatureProperty> createSignatureProperty(SignatureProperty value) {
        return new JAXBElement<SignatureProperty>(_SignatureProperty_QNAME, SignatureProperty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartAsProxyWithDigitalSignatureRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "startAsProxyWithDigitalSignatureRequest")
    public JAXBElement<StartAsProxyWithDigitalSignatureRequest> createStartAsProxyWithDigitalSignatureRequest(StartAsProxyWithDigitalSignatureRequest value) {
        return new JAXBElement<StartAsProxyWithDigitalSignatureRequest>(_StartAsProxyWithDigitalSignatureRequest_QNAME, StartAsProxyWithDigitalSignatureRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWorkEntriesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getWorkEntriesRequest")
    public JAXBElement<GetWorkEntriesRequest> createGetWorkEntriesRequest(GetWorkEntriesRequest value) {
        return new JAXBElement<GetWorkEntriesRequest>(_GetWorkEntriesRequest_QNAME, GetWorkEntriesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGraphRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getGraphRequest")
    public JAXBElement<GetGraphRequest> createGetGraphRequest(GetGraphRequest value) {
        return new JAXBElement<GetGraphRequest>(_GetGraphRequest_QNAME, GetGraphRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVersionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getVersionResponse")
    public JAXBElement<GetVersionResponse> createGetVersionResponse(GetVersionResponse value) {
        return new JAXBElement<GetVersionResponse>(_GetVersionResponse_QNAME, GetVersionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProvisioningStatusesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProvisioningStatusesRequest")
    public JAXBElement<GetProvisioningStatusesRequest> createGetProvisioningStatusesRequest(GetProvisioningStatusesRequest value) {
        return new JAXBElement<GetProvisioningStatusesRequest>(_GetProvisioningStatusesRequest_QNAME, GetProvisioningStatusesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartWithCorrelationIdRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "startWithCorrelationIdRequest")
    public JAXBElement<StartWithCorrelationIdRequest> createStartWithCorrelationIdRequest(StartWithCorrelationIdRequest value) {
        return new JAXBElement<StartWithCorrelationIdRequest>(_StartWithCorrelationIdRequest_QNAME, StartWithCorrelationIdRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TWorkEntryQuery }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "WorkEntryQuery")
    public JAXBElement<TWorkEntryQuery> createWorkEntryQuery(TWorkEntryQuery value) {
        return new JAXBElement<TWorkEntryQuery>(_WorkEntryQuery_QNAME, TWorkEntryQuery.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvisioningRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "ProvisioningRequest")
    public JAXBElement<ProvisioningRequest> createProvisioningRequest(ProvisioningRequest value) {
        return new JAXBElement<ProvisioningRequest>(_ProvisioningRequest_QNAME, ProvisioningRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetRoleRequestStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "setRoleRequestStatusResponse")
    public JAXBElement<SetRoleRequestStatusResponse> createSetRoleRequestStatusResponse(SetRoleRequestStatusResponse value) {
        return new JAXBElement<SetRoleRequestStatusResponse>(_SetRoleRequestStatusResponse_QNAME, SetRoleRequestStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WorkEntry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "WorkEntry")
    public JAXBElement<WorkEntry> createWorkEntry(WorkEntry value) {
        return new JAXBElement<WorkEntry>(_WorkEntry_QNAME, WorkEntry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCommentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "addCommentResponse")
    public JAXBElement<AddCommentResponse> createAddCommentResponse(AddCommentResponse value) {
        return new JAXBElement<AddCommentResponse>(_AddCommentResponse_QNAME, AddCommentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWorkRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getWorkRequest")
    public JAXBElement<GetWorkRequest> createGetWorkRequest(GetWorkRequest value) {
        return new JAXBElement<GetWorkRequest>(_GetWorkRequest_QNAME, GetWorkRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ForwardAsProxyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "forwardAsProxyRequest")
    public JAXBElement<ForwardAsProxyRequest> createForwardAsProxyRequest(ForwardAsProxyRequest value) {
        return new JAXBElement<ForwardAsProxyRequest>(_ForwardAsProxyRequest_QNAME, ForwardAsProxyRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvisioningStatusArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "ProvisioningStatusArray")
    public JAXBElement<ProvisioningStatusArray> createProvisioningStatusArray(ProvisioningStatusArray value) {
        return new JAXBElement<ProvisioningStatusArray>(_ProvisioningStatusArray_QNAME, ProvisioningStatusArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByInitiatorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByInitiatorResponse")
    public JAXBElement<GetProcessesByInitiatorResponse> createGetProcessesByInitiatorResponse(GetProcessesByInitiatorResponse value) {
        return new JAXBElement<GetProcessesByInitiatorResponse>(_GetProcessesByInitiatorResponse_QNAME, GetProcessesByInitiatorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetRoleRequestStatusRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "setRoleRequestStatusRequest")
    public JAXBElement<SetRoleRequestStatusRequest> createSetRoleRequestStatusRequest(SetRoleRequestStatusRequest value) {
        return new JAXBElement<SetRoleRequestStatusRequest>(_SetRoleRequestStatusRequest_QNAME, SetRoleRequestStatusRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnclaimRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "unclaimRequest")
    public JAXBElement<UnclaimRequest> createUnclaimRequest(UnclaimRequest value) {
        return new JAXBElement<UnclaimRequest>(_UnclaimRequest_QNAME, UnclaimRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllProcessesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getAllProcessesRequest")
    public JAXBElement<GetAllProcessesRequest> createGetAllProcessesRequest(GetAllProcessesRequest value) {
        return new JAXBElement<GetAllProcessesRequest>(_GetAllProcessesRequest_QNAME, GetAllProcessesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllProvisioningRequestsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getAllProvisioningRequestsResponse")
    public JAXBElement<GetAllProvisioningRequestsResponse> createGetAllProvisioningRequestsResponse(GetAllProvisioningRequestsResponse value) {
        return new JAXBElement<GetAllProvisioningRequestsResponse>(_GetAllProvisioningRequestsResponse_QNAME, GetAllProvisioningRequestsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWebServiceActivityTimeoutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getWebServiceActivityTimeoutResponse")
    public JAXBElement<GetWebServiceActivityTimeoutResponse> createGetWebServiceActivityTimeoutResponse(GetWebServiceActivityTimeoutResponse value) {
        return new JAXBElement<GetWebServiceActivityTimeoutResponse>(_GetWebServiceActivityTimeoutResponse_QNAME, GetWebServiceActivityTimeoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProvisioningCategoriesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProvisioningCategoriesRequest")
    public JAXBElement<GetProvisioningCategoriesRequest> createGetProvisioningCategoriesRequest(GetProvisioningCategoriesRequest value) {
        return new JAXBElement<GetProvisioningCategoriesRequest>(_GetProvisioningCategoriesRequest_QNAME, GetProvisioningCategoriesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByApprovalStatusRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByApprovalStatusRequest")
    public JAXBElement<GetProcessesByApprovalStatusRequest> createGetProcessesByApprovalStatusRequest(GetProcessesByApprovalStatusRequest value) {
        return new JAXBElement<GetProcessesByApprovalStatusRequest>(_GetProcessesByApprovalStatusRequest_QNAME, GetProcessesByApprovalStatusRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessResponse")
    public JAXBElement<GetProcessResponse> createGetProcessResponse(GetProcessResponse value) {
        return new JAXBElement<GetProcessResponse>(_GetProcessResponse_QNAME, GetProcessResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCommentsByUserRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getCommentsByUserRequest")
    public JAXBElement<GetCommentsByUserRequest> createGetCommentsByUserRequest(GetCommentsByUserRequest value) {
        return new JAXBElement<GetCommentsByUserRequest>(_GetCommentsByUserRequest_QNAME, GetCommentsByUserRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesByQueryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProcessesByQueryRequest")
    public JAXBElement<GetProcessesByQueryRequest> createGetProcessesByQueryRequest(GetProcessesByQueryRequest value) {
        return new JAXBElement<GetProcessesByQueryRequest>(_GetProcessesByQueryRequest_QNAME, GetProcessesByQueryRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetCompletedProcessTimeoutRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "setCompletedProcessTimeoutRequest")
    public JAXBElement<SetCompletedProcessTimeoutRequest> createSetCompletedProcessTimeoutRequest(SetCompletedProcessTimeoutRequest value) {
        return new JAXBElement<SetCompletedProcessTimeoutRequest>(_SetCompletedProcessTimeoutRequest_QNAME, SetCompletedProcessTimeoutRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WorkEntryArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "WorkEntryArray")
    public JAXBElement<WorkEntryArray> createWorkEntryArray(WorkEntryArray value) {
        return new JAXBElement<WorkEntryArray>(_WorkEntryArray_QNAME, WorkEntryArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetQuorumForWorkTaskResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getQuorumForWorkTaskResponse")
    public JAXBElement<GetQuorumForWorkTaskResponse> createGetQuorumForWorkTaskResponse(GetQuorumForWorkTaskResponse value) {
        return new JAXBElement<GetQuorumForWorkTaskResponse>(_GetQuorumForWorkTaskResponse_QNAME, GetQuorumForWorkTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWorkEntriesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getWorkEntriesResponse")
    public JAXBElement<GetWorkEntriesResponse> createGetWorkEntriesResponse(GetWorkEntriesResponse value) {
        return new JAXBElement<GetWorkEntriesResponse>(_GetWorkEntriesResponse_QNAME, GetWorkEntriesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFormDefinitionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getFormDefinitionResponse")
    public JAXBElement<GetFormDefinitionResponse> createGetFormDefinitionResponse(GetFormDefinitionResponse value) {
        return new JAXBElement<GetFormDefinitionResponse>(_GetFormDefinitionResponse_QNAME, GetFormDefinitionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProvisioningCategoriesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "getProvisioningCategoriesResponse")
    public JAXBElement<GetProvisioningCategoriesResponse> createGetProvisioningCategoriesResponse(GetProvisioningCategoriesResponse value) {
        return new JAXBElement<GetProvisioningCategoriesResponse>(_GetProvisioningCategoriesResponse_QNAME, GetProvisioningCategoriesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartWithDigitalSignatureRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "startWithDigitalSignatureRequest")
    public JAXBElement<StartWithDigitalSignatureRequest> createStartWithDigitalSignatureRequest(StartWithDigitalSignatureRequest value) {
        return new JAXBElement<StartWithDigitalSignatureRequest>(_StartWithDigitalSignatureRequest_QNAME, StartWithDigitalSignatureRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ForwardAsProxyWithDigitalSignatureResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "forwardAsProxyWithDigitalSignatureResponse")
    public JAXBElement<ForwardAsProxyWithDigitalSignatureResponse> createForwardAsProxyWithDigitalSignatureResponse(ForwardAsProxyWithDigitalSignatureResponse value) {
        return new JAXBElement<ForwardAsProxyWithDigitalSignatureResponse>(_ForwardAsProxyWithDigitalSignatureResponse_QNAME, ForwardAsProxyWithDigitalSignatureResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReassignAllProcessesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "reassignAllProcessesRequest")
    public JAXBElement<ReassignAllProcessesRequest> createReassignAllProcessesRequest(ReassignAllProcessesRequest value) {
        return new JAXBElement<ReassignAllProcessesRequest>(_ReassignAllProcessesRequest_QNAME, ReassignAllProcessesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetCompletedProcessTimeoutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "setCompletedProcessTimeoutResponse")
    public JAXBElement<SetCompletedProcessTimeoutResponse> createSetCompletedProcessTimeoutResponse(SetCompletedProcessTimeoutResponse value) {
        return new JAXBElement<SetCompletedProcessTimeoutResponse>(_SetCompletedProcessTimeoutResponse_QNAME, SetCompletedProcessTimeoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "engineId", scope = TProcessInfoQuery.class)
    public JAXBElement<String> createTProcessInfoQueryEngineId(String value) {
        return new JAXBElement<String>(_TProcessInfoQueryEngineId_QNAME, String.class, TProcessInfoQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TTime }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "creationTime", scope = TProcessInfoQuery.class)
    public JAXBElement<TTime> createTProcessInfoQueryCreationTime(TTime value) {
        return new JAXBElement<TTime>(_TProcessInfoQueryCreationTime_QNAME, TTime.class, TProcessInfoQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "resourceType", scope = TProcessInfoQuery.class)
    public JAXBElement<String> createTProcessInfoQueryResourceType(String value) {
        return new JAXBElement<String>(_TProcessInfoQueryResourceType_QNAME, String.class, TProcessInfoQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "recipient", scope = TProcessInfoQuery.class)
    public JAXBElement<String> createTProcessInfoQueryRecipient(String value) {
        return new JAXBElement<String>(_TProvisioningStatusQueryRecipient_QNAME, String.class, TProcessInfoQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TTime }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "completionTime", scope = TProcessInfoQuery.class)
    public JAXBElement<TTime> createTProcessInfoQueryCompletionTime(TTime value) {
        return new JAXBElement<TTime>(_TProcessInfoQueryCompletionTime_QNAME, TTime.class, TProcessInfoQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "correlationId", scope = TProcessInfoQuery.class)
    public JAXBElement<String> createTProcessInfoQueryCorrelationId(String value) {
        return new JAXBElement<String>(_TProcessInfoQueryCorrelationId_QNAME, String.class, TProcessInfoQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "approvalStatus", scope = TProcessInfoQuery.class)
    public JAXBElement<Integer> createTProcessInfoQueryApprovalStatus(Integer value) {
        return new JAXBElement<Integer>(_TProcessInfoQueryApprovalStatus_QNAME, Integer.class, TProcessInfoQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "processStatus", scope = TProcessInfoQuery.class)
    public JAXBElement<Integer> createTProcessInfoQueryProcessStatus(Integer value) {
        return new JAXBElement<Integer>(_TProcessInfoQueryProcessStatus_QNAME, Integer.class, TProcessInfoQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "processId", scope = TProcessInfoQuery.class)
    public JAXBElement<String> createTProcessInfoQueryProcessId(String value) {
        return new JAXBElement<String>(_TProcessInfoQueryProcessId_QNAME, String.class, TProcessInfoQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "requestId", scope = TProcessInfoQuery.class)
    public JAXBElement<String> createTProcessInfoQueryRequestId(String value) {
        return new JAXBElement<String>(_TProvisioningStatusQueryRequestId_QNAME, String.class, TProcessInfoQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "initiator", scope = TProcessInfoQuery.class)
    public JAXBElement<String> createTProcessInfoQueryInitiator(String value) {
        return new JAXBElement<String>(_TProcessInfoQueryInitiator_QNAME, String.class, TProcessInfoQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TTime }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "creationTime", scope = TWorkEntryQuery.class)
    public JAXBElement<TTime> createTWorkEntryQueryCreationTime(TTime value) {
        return new JAXBElement<TTime>(_TProcessInfoQueryCreationTime_QNAME, TTime.class, TWorkEntryQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "addressee", scope = TWorkEntryQuery.class)
    public JAXBElement<String> createTWorkEntryQueryAddressee(String value) {
        return new JAXBElement<String>(_TWorkEntryQueryAddressee_QNAME, String.class, TWorkEntryQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "owner", scope = TWorkEntryQuery.class)
    public JAXBElement<String> createTWorkEntryQueryOwner(String value) {
        return new JAXBElement<String>(_TWorkEntryQueryOwner_QNAME, String.class, TWorkEntryQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "recipient", scope = TWorkEntryQuery.class)
    public JAXBElement<String> createTWorkEntryQueryRecipient(String value) {
        return new JAXBElement<String>(_TProvisioningStatusQueryRecipient_QNAME, String.class, TWorkEntryQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "proxyFor", scope = TWorkEntryQuery.class)
    public JAXBElement<String> createTWorkEntryQueryProxyFor(String value) {
        return new JAXBElement<String>(_TWorkEntryQueryProxyFor_QNAME, String.class, TWorkEntryQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TTime }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "completionTime", scope = TWorkEntryQuery.class)
    public JAXBElement<TTime> createTWorkEntryQueryCompletionTime(TTime value) {
        return new JAXBElement<TTime>(_TProcessInfoQueryCompletionTime_QNAME, TTime.class, TWorkEntryQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "activityId", scope = TWorkEntryQuery.class)
    public JAXBElement<String> createTWorkEntryQueryActivityId(String value) {
        return new JAXBElement<String>(_TProvisioningStatusQueryActivityId_QNAME, String.class, TWorkEntryQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TTime }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "expTime", scope = TWorkEntryQuery.class)
    public JAXBElement<TTime> createTWorkEntryQueryExpTime(TTime value) {
        return new JAXBElement<TTime>(_TWorkEntryQueryExpTime_QNAME, TTime.class, TWorkEntryQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "priority", scope = TWorkEntryQuery.class)
    public JAXBElement<Integer> createTWorkEntryQueryPriority(Integer value) {
        return new JAXBElement<Integer>(_TWorkEntryQueryPriority_QNAME, Integer.class, TWorkEntryQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "initiator", scope = TWorkEntryQuery.class)
    public JAXBElement<String> createTWorkEntryQueryInitiator(String value) {
        return new JAXBElement<String>(_TProcessInfoQueryInitiator_QNAME, String.class, TWorkEntryQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "status", scope = TWorkEntryQuery.class)
    public JAXBElement<Integer> createTWorkEntryQueryStatus(Integer value) {
        return new JAXBElement<Integer>(_TProvisioningStatusQueryStatus_QNAME, Integer.class, TWorkEntryQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "requestId", scope = TWorkEntryQuery.class)
    public JAXBElement<String> createTWorkEntryQueryRequestId(String value) {
        return new JAXBElement<String>(_TProvisioningStatusQueryRequestId_QNAME, String.class, TWorkEntryQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.novell.com/provisioning/service", name = "processId", scope = TWorkEntryQuery.class)
    public JAXBElement<String> createTWorkEntryQueryProcessId(String value) {
        return new JAXBElement<String>(_TProcessInfoQueryProcessId_QNAME, String.class, TWorkEntryQuery.class, value);
    }

}
