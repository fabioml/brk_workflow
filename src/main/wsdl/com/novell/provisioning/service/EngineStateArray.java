
package com.novell.provisioning.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EngineStateArray complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EngineStateArray">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="engineStates" type="{http://www.novell.com/provisioning/service}EngineState" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EngineStateArray", propOrder = {
    "engineStates"
})
public class EngineStateArray {

    protected List<EngineState> engineStates;

    /**
     * Gets the value of the engineStates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the engineStates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEngineStates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EngineState }
     * 
     * 
     */
    public List<EngineState> getEngineStates() {
        if (engineStates == null) {
            engineStates = new ArrayList<EngineState>();
        }
        return this.engineStates;
    }

}
