
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getProvisioningStatusesRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getProvisioningStatusesRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.novell.com/provisioning/service}ProvisioningStatusQuery"/>
 *         &lt;element name="arg1" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getProvisioningStatusesRequest", propOrder = {
    "provisioningStatusQuery",
    "arg1"
})
public class GetProvisioningStatusesRequest {

    @XmlElement(name = "ProvisioningStatusQuery", required = true)
    protected TProvisioningStatusQuery provisioningStatusQuery;
    protected int arg1;

    /**
     * Gets the value of the provisioningStatusQuery property.
     * 
     * @return
     *     possible object is
     *     {@link TProvisioningStatusQuery }
     *     
     */
    public TProvisioningStatusQuery getProvisioningStatusQuery() {
        return provisioningStatusQuery;
    }

    /**
     * Sets the value of the provisioningStatusQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link TProvisioningStatusQuery }
     *     
     */
    public void setProvisioningStatusQuery(TProvisioningStatusQuery value) {
        this.provisioningStatusQuery = value;
    }

    /**
     * Gets the value of the arg1 property.
     * 
     */
    public int getArg1() {
        return arg1;
    }

    /**
     * Sets the value of the arg1 property.
     * 
     */
    public void setArg1(int value) {
        this.arg1 = value;
    }

}
