
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getEngineStateResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getEngineStateResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.novell.com/provisioning/service}EngineState"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getEngineStateResponse", propOrder = {
    "engineState"
})
public class GetEngineStateResponse {

    @XmlElement(name = "EngineState", required = true)
    protected EngineState engineState;

    /**
     * Gets the value of the engineState property.
     * 
     * @return
     *     possible object is
     *     {@link EngineState }
     *     
     */
    public EngineState getEngineState() {
        return engineState;
    }

    /**
     * Sets the value of the engineState property.
     * 
     * @param value
     *     allowed object is
     *     {@link EngineState }
     *     
     */
    public void setEngineState(EngineState value) {
        this.engineState = value;
    }

}
