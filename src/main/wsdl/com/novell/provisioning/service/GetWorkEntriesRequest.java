
package com.novell.provisioning.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getWorkEntriesRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getWorkEntriesRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.novell.com/provisioning/service}WorkEntryQuery"/>
 *         &lt;element name="arg1" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getWorkEntriesRequest", propOrder = {
    "workEntryQuery",
    "arg1"
})
public class GetWorkEntriesRequest {

    @XmlElement(name = "WorkEntryQuery", required = true)
    protected TWorkEntryQuery workEntryQuery;
    protected int arg1;

    /**
     * Gets the value of the workEntryQuery property.
     * 
     * @return
     *     possible object is
     *     {@link TWorkEntryQuery }
     *     
     */
    public TWorkEntryQuery getWorkEntryQuery() {
        return workEntryQuery;
    }

    /**
     * Sets the value of the workEntryQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link TWorkEntryQuery }
     *     
     */
    public void setWorkEntryQuery(TWorkEntryQuery value) {
        this.workEntryQuery = value;
    }

    /**
     * Gets the value of the arg1 property.
     * 
     */
    public int getArg1() {
        return arg1;
    }

    /**
     * Sets the value of the arg1 property.
     * 
     */
    public void setArg1(int value) {
        this.arg1 = value;
    }

}
